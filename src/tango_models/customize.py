import csv

import cobra
import numpy as np
import pandas as pd

from . import dFBA

##### customize species #####

def block_flux_reaction(param,sp_reac,model_list):
    """
    - block the flux of specify reaction
    - if bounds is specified, adjust the flux to this bounds
    - dico_blocked --> species : reac
    - dico_spe --> species : reac : bounds
    """
    param['spe_reac'] = {mod_id : {reac_id:{"lower_bound": sp_reac[mod_id]['spe_reaction'][reac_id]['lower_bound'],
                                            "upper_bound": sp_reac[mod_id]['spe_reaction'][reac_id]['upper_bound']} 
                                   for reac_id in sp_reac[mod_id]['spe_reaction'].keys()}
                         for mod_id in sp_reac.keys()}
    for model in model_list:
        for r in sp_reac[model.id]['reactions_blocked']:
            param['model_dict'][model.id].reactions.get_by_id(r).bounds=(0,0)
                # print(param['model_dict'][model.id].reactions.get_by_id(r),param['model_dict'][model.id].reactions.get_by_id(r).bounds)
        for r_idx,r in enumerate(sp_reac[model.id]['spe_reaction'].keys()):
            param['model_dict'][model.id].reactions.get_by_id(r).bounds=(sp_reac[model.id]['spe_reaction'][r]['lower_bound'],sp_reac[model.id]['spe_reaction'][r]['upper_bound'])
    

def media_individual_species(param,media_reac,model_list,sp_reac):
    """
    define a media for the growth of individual species 
    """
    for model in model_list:
        for r in sp_reac[model.id]['reactions_0']:
            media = param['model_dict'][model.id].medium
            media[r] = 0
            param['model_dict'][model.id].medium = media

        for r in sp_reac[model.id]['reactions_10']:
            media = param['model_dict'][model.id].medium
            media[r] = 10
            param['model_dict'][model.id].medium = media

def SetO2Bound(param,model_list):
    for model in model_list:
        o2=param['oxygene'][model.id]
        o2_reac = model.reactions.get_by_id('EX_o2_e')
        o2_reac.lower_bound=-o2


def ComputeProductionBoundsFromData(param, model_list, data_key=['ExpMetabolites','ExpGrowth'], products=['EX_succ_e','EX_ppa_e','EX_ac_e'], substrate=['EX_lac__D_e','EX_lac__L_e']):
    """
    Compute FBA bounds from an heuristic (see below)
    """
    for model in model_list:
        if model.id == 'freudenreichii':
            try:
                A = pd.read_csv(param['data_optim'][model.id]['ExpMetabolites']['path'],sep=',',header=0).set_index('time')
            except:
                raise NameError("invalid data path")        
            if 'fake' in param['data_optim'][model.id]['ExpMetabolites']['path']:
                Conc=A.pivot_table(columns=['metabolite'],index='time')['mean']
                Conc=Conc.apply(lambda x:dFBA.convert_g_kg_in_mmol_g(x,molar_mass=param['molar_mass'][x.name]) if x.name not in ['time'] else x, axis=0)
                Growth = 10**Conc[model.id]
            else:
                Conc=A.pivot_table(columns=['metabolite'],index='time')['mean']
                Conc=Conc.apply(lambda x:dFBA.convert_g_kg_in_mmol_g(x,molar_mass=param['molar_mass'][x.name]) if x.name not in ['time'] else x, axis=0)
                try:
                    B = pd.read_csv(param['data_optim'][model.id]['ExpGrowth']['path'],sep=',',header=0).set_index('time')
                except:
                    raise NameError("invalid data path")    
                Growth=B[B['type']=='numbering'].pivot_table(columns=['species','replicate'],index='time')[('value','freudenreichii')].mean(axis=1)

                cfu_to_gramm=0.33e-12
                Growth = (cfu_to_gramm*Growth).dropna()


                ## We compute fba bounds for produced compounds with the following heuristic:
                ## if the production time course follows the following equation
                ## dt c = F_c(c,b) b
                ## where c is the compound concentration, F_c, the FBA result for c and b the bacteria concentration, then, assuming that F_c is constant, we have
                ##  c(T_final)-c(T_0) = F_c \int_{t_0}^{T_final} b(t) dt.
                ## Computing AUC = \int_{t_0}^{T_final} b(t) dt then allow to compute F_c.
            
                ## The same heuristic can be used to get the intrinsic_flux for the substrates.
            
                ## To prevent 
            time = np.array([i for i in Growth.index])
            #Final time for growth experiment in plateau phase is 89h
            #time[-1]=param['K_lac'][model.id]
            #print(time)

            time_interval=np.array([float(i) for i in time[1:]-time[:-1]])

            mean_value_growth=(Growth.values[1:]+Growth.values[:-1])/2.0

            # an exponential consumption of lactate is assumed in the data => compute the coefficient            
            coeff_exponential_consumption = (np.log(Conc['EX_lac__L_e'].iloc[-1]) - np.log(Conc['EX_lac__L_e'].iloc[0]))/(float(time[-1])-float(time[0]))
            value_lac= np.array([Conc['EX_lac__L_e'].iloc[0]*np.exp(coeff_exponential_consumption*float(i)) for i in time]) 

            #value_comtois = value_lac/(param['K_lac'][model.id]*Growth.values+value_lac)
            #mean_value_comtois = (value_comtois[1:] + value_comtois[:-1])/2.0

            #AUC = np.sum(time_interval*mean_value_growth*mean_value_comtois)
            AUC_growth = np.sum(time_interval*mean_value_growth)
            AUC_exp = np.sum(time_interval[:2]*mean_value_growth[:2])

            param['intrinsic_flux_product']={}
            alpha=1.08
            for prod in products:
                if prod in ['EX_ac_e','EX_ppa_e','EX_succ_e']:
                    param['intrinsic_flux_product'][prod]=alpha*(Conc[prod].iloc[-1]-Conc[prod].iloc[0])/AUC_growth
                else:
                    param['intrinsic_flux_product'][prod]=alpha*(Conc[prod].iloc[-1]-Conc[prod].iloc[0])/AUC_growth
            for key in substrate:
                param['intrinsic_flux_product'][key]=alpha*(Conc['EX_lac__L_e'].iloc[-1]-Conc['EX_lac__L_e'].iloc[0])/AUC_growth/2 #division by 2 because lac__L and lac__D are pooled during dosage
            param['intrinsic_flux_product']['EX_lcts_e']=alpha*(-dFBA.convert_g_kg_in_mmol_g(param['lactose_concentration'][model.id],molar_mass=param['molar_mass']['EX_lcts_e'])/AUC_exp)
            param['intrinsic_flux_product']['products']=products
        else:
            print(10*'*'+'\n'+'Computation of production bounds from data is only implemented for Freudenreichii'+'\n'+10*'*')

