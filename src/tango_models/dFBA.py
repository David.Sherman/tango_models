import gzip
import math
import pickle
import sys
import time

import cobra
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from cobra.flux_analysis.parsimonious import pfba
from scipy import interpolate

from . import optim, simulation


class Mock_sol():
    """ 
    This empty class is introduced to encapsulate solutions obtained with the function cobra.util.add_lexicographic_constraints, which returns a pandas serie, with equivalent fields of the class FBA results returned by the function model.optimize()
    """
    def __init__(self):
        self.fluxes=None
        self.status = None
        pass


def DFBA(param,times=None,DM=None,b=None,com=None,transcrip=None,opt=None,VERBOSE=False,lactic_acid='total',freud_sim=None):
    """ Run the Dfba simulation. Compute at each time step a FBA model to calcule fluxes.

    Args:
        param (dict): dictionnary of all essential parameters used in the dFBA and optim program
        times (ndarray, optional): array of interesting time step. Defaults to None.
        DM (ndarray or panda series): array of dry matter pourcentage.
        b (str, optional): id of the current bacterium. Defaults to None.
        com (Bool, optional): used to compute community dFBA. Defaults to None.

    Returns:
        ndarray: array of all computed fluxes 
    """   
    output_flag=param['output_flag']
    id_subs=param['id_subs']
    id_bact=param['id_bact']
    dt = param['dt']
    Tf = param['Tf']
    if times is not None:
        Tf = np.min(np.array([Tf,np.max(np.array(times))]))
    y = param['y0'].copy()
    epsilon=1e-10
    result=[]
    if com:
        flux = {el:[] for el in param['bact']}


    i_eval=0

    t=np.array([0])
    Moulding_concentration_flag=True


    result.append(np.concatenate([t,y]))
    while t[0] <Tf: # Time loop
        flux_fba=np.zeros((len(param['compound_name']),))    
        curr_flux = {b:np.zeros((len(param['compound_name']),0)) for b in param['bact']} 
        qs = {b:0.0 for b in param['bact']}
        death_flux = {b:0.0 for b in param['bact']}
        for curr_b in param['bact']:
                        
            curr_flux[curr_b],curr_import,flag_err = GetFlux(y,t[0],param,curr_b,com=com,times=times,lactic_acid=lactic_acid)
            
            qs[curr_b] = quorumSensing(y,t[0],param,bact_id=curr_b)
            
            if curr_b in ['lactis','plantarum']:
                idx = np.array([param['Y_id'][e] for e in [curr_b]])#,'EX_ac_e']]) #
                curr_flux[curr_b][idx]*=qs[curr_b]              
            elif curr_b in ['freudenreichii']:                
                idx = np.array([param['Y_id'][e] for e in [curr_b]]) #
                curr_flux[curr_b][idx]*=qs[curr_b]                
            else:
                raise NameError('Wrong bacteria ID')
            flux_fba[:]+=curr_flux[curr_b][:]             

        if com:
            for b in param['bact']:
                flux[b].append(np.concatenate([t,curr_flux[b]]))
        y[param['Y_id']['ph']]= getLactatePh(y[param['Y_id']['EX_lac__L_e']],y[param['Y_id']['EX_lac__D_e']],c1 = param['c1'][curr_b],c2 = param['c2'][curr_b],lactic_acid=lactic_acid)


        flux_tot = flux_fba
        id_pos = flux_tot>=0.0
        id_neg = flux_tot<0.0
        
        y[id_neg] = y[id_neg]/(1-dt*flux_tot[id_neg]/(y[id_neg]+epsilon))
        y[id_pos] = y[id_pos]+dt*flux_tot[id_pos]
        
        if VERBOSE:    
            for key in param['Y_id'].keys():
                print(t[0],'/',Tf,key,y[param['Y_id'][key]])
            print('*******************')

        t=t+dt
        for cpd in param['compound_name']:
            if y[param['Y_id'][cpd]] < 10**(-9):
                y[param['Y_id'][cpd]] = 10**(-9)
        result.append(np.concatenate([t,y]))
        
        i_eval+=1       
        if com: 
            if t[0] > times.loc['Aff4s'][0]:
                dt = 10*param['dt']
            elif t[0] > times.loc['AS'][0] and t[0] <= times.loc['Aff4s'][0]:
                dt = 3*param['dt']
    result=np.array(result)
    if com:
        for b in param['bact']:
            flux[b] = np.array(flux[b])
        with gzip.open("results/result_community_model_whole.pkz",'wb') as f:
            pickle.dump({"result":result,"param":param,"flux":flux},f)
    else:
        if not opt:
            if freud_sim == None:
                freud_sim=''
            if param["bact"][0] in ['lactis','plantarum']:
                file_n = "results/result_pure_culture_model_"+param["bact"][0]+".pkz"
            else:
                file_n = "results/result_pure_culture_model_"+param["bact"][0]+'_'+freud_sim+".pkz"
            with gzip.open(file_n,'wb') as f:
                pickle.dump({"result":result,"param":param},f)

    if times is not None:    
        if com:
            interest_time = times.loc[param['interest_value']]
            interpolation = interpolate.interp1d(result[:,0], result[:,1:],axis=0)
            result_y = interpolation(interest_time['time'])
            result=pd.concat([interest_time,pd.DataFrame(result_y[:,[param['Y_id'][key] for key in param['Y_id'].keys()]],columns=[key for key in param['Y_id'].keys()],index=interest_time.index)],axis=1)
        else:
            interpolation = interpolate.interp1d(result[:,0], result[:,1:],axis=0)
            result_y = interpolation(times)
            result=np.hstack([pd.DataFrame(times),result_y])            
        return result

    return result



def GetFlux(Y,t,param,bact_id,com=bool(),times=None, lactic_acid='total',FLUX_OUTPUT=True):

    """ This function computes for a given external nutritional environment (i.e. concentration of limiting substrates) the FBA optimal_flux : i.e. the import flux and the corresponding abcterial growth rate. 

    The bounds on the import flux are computed with the following heuristic:
    During a time step limit_dt, the concentration C_lim of a limiting compound is available to the community. The import function of every bacterial strains will be limited by 1) intrinsic import capabilities, 2) community-wide ressource sharing.
    1) the intrinsic import flux will be callibrated on the individual growth curves and will be fixed.
    2) The community-wide ressource sharing will be dynamic. Noting C_total_bact the concentration of total bacterial community, bacteria will never import more limiting compound that 
    
    F_max = C_lim/(limit_dt * C_total_bact). 
    F_max is the maximal import flux by biomass unit during the time step limit_dt. The underlying assumption is that all the bacteria share the same limiting substrate amount, and will all have the same internal 
    regulation mechanisms allowing a fair share of ressource amoung the bacteria of the community.

    Args:
        Y (ndarray): Screening compounds
        t (float): current time
        param (dict): dictionnary of all essential parameters used in the dFBA and optim program
        bact_id (str): id of the current bacterium

    Returns:
        float,str : current flux and the error flag of computed fluxes
    """    
    epsilon = 1e-6

    flag_err=False

    curr_mod = param['model_dict'][bact_id]
    optimal_flux = np.zeros((len(param['compound_name']),))
    current_bounds = np.zeros((len(param['compound_name']),))

    if np.any(Y<0):
        reverse_dict = {i_x:i for i,i_x in param['Y_id'].items()}
        where_tmp=np.where(Y<0)
        print(where_tmp)
        raise NameError('negative value : for output '+' '.join([reverse_dict[a] for a in where_tmp[0]])+' with values '+' '.join([str(Y[i]) for i in where_tmp[0]]))

    ############## species and metabolite dependant bounds update #################
    ###############################################################################

    with curr_mod as mod: 
          
        ######### bounds for compounds that are screened dynamically ##########
        for reac in param['Screened_Compounds'][mod.id]: # for each limiting substrate, we define the new lower bound  
            if reac.id in mod.reactions:
                y_bact=np.sum(Y[param['bact_Y_id_by_screened_compound'][reac.id]]) #compute total biomass of micro-organisms presenting the current reaction
                reac_mod=mod.reactions.get_by_id(reac.id)
                
                ##############################
                #default reaction modification
                reac_mod.lower_bound = max(-Y[param['Y_id'][reac.id]]/((param['limit_dt']*y_bact)+epsilon),param['intrinsic_flux'][mod.id][reac.id]) # limiting substrate concentration dependent bound






                
                ##############################
                #Additionnal reaction modification                
                ######### lactis #########
                if mod.id in ['lactis']:
                    if 'lcts' in reac_mod.id:
                        undissociatedLacticAcid=getUndissociatedAcidLactiqueConcentration(Y[param['Y_id']['EX_lac__L_e']],
                                                                             Y[param['Y_id']['EX_lac__D_e']],
                                                                             param['c1'][mod.id],
                                                                             param['c2'][mod.id],
                                                                             lactic_acid=lactic_acid)
                        reac_mod.lower_bound=max(-Y[param['Y_id'][reac_mod.id]]/((param['limit_dt']*y_bact)+epsilon)-epsilon,-param['vmax_lactose'][mod.id]*10**(-param['k_lactate'][mod.id]*undissociatedLacticAcid)-param['vmin_lactose'][mod.id])


                    if 'lac_' in reac_mod.id:
                        undissociatedLacticAcid=getUndissociatedAcidLactiqueConcentration(Y[param['Y_id']['EX_lac__L_e']],
                                                                             Y[param['Y_id']['EX_lac__D_e']],
                                                                             param['c1'][mod.id],
                                                                             param['c2'][mod.id],
                                                                             lactic_acid=lactic_acid)                    
                        reac_mod.lower_bound = 0.0
                        reac_mod.upper_bound= min(Y[param['Y_id']['EX_lcts_e']]/((param['limit_dt']*np.sum(Y[param['bact_Y_id_by_screened_compound']['EX_lcts_e']]))+epsilon),(param['vmax_lactose'][mod.id]*10**(-param['k_lactate'][mod.id]*undissociatedLacticAcid) + param['vmin_lactose'][mod.id]))*4.0

                    if 'EX_ac_e' in reac_mod.id:
                        reac_mod.lower_bound = 0.0
                        reac_mod.upper_bound= min(Y[param['Y_id']['EX_lcts_e']]/((param['limit_dt']*np.sum(Y[param['bact_Y_id_by_screened_compound']['EX_lcts_e']]))+epsilon),param['intrinsic_flux'][mod.id][reac.id])

                    
                ######## plantarum #######
                elif mod.id in ['plantarum']:
                
                    if 'lcts' in reac_mod.id:
                        undissociatedLacticAcid=getUndissociatedAcidLactiqueConcentration(Y[param['Y_id']['EX_lac__L_e']],
                                                                             Y[param['Y_id']['EX_lac__D_e']],
                                                                             param['c1'][mod.id],
                                                                             param['c2'][mod.id],
                                                                             lactic_acid=lactic_acid)
                        reac_mod.lower_bound=max(-Y[param['Y_id'][reac_mod.id]]/((param['limit_dt']*y_bact)+epsilon),-param['vmax_lactose'][mod.id]*10**(-param['k_lactate'][mod.id]*undissociatedLacticAcid)-param['vmin_lactose'][mod.id])
                    if 'lac_' in reac_mod.id:
                        undissociatedLacticAcid=getUndissociatedAcidLactiqueConcentration(Y[param['Y_id']['EX_lac__L_e']],
                                                                             Y[param['Y_id']['EX_lac__D_e']],
                                                                             param['c1'][mod.id],
                                                                             param['c2'][mod.id],
                                                                             lactic_acid=lactic_acid)                    
                        reac_mod.lower_bound = 0.0
                        reac_mod.upper_bound= min(Y[param['Y_id']['EX_lcts_e']]/((param['limit_dt']*np.sum(Y[param['bact_Y_id_by_screened_compound']['EX_lcts_e']]))+epsilon),param['vmax_lactose'][mod.id]*10**(-param['k_lactate'][mod.id]*undissociatedLacticAcid) + param['vmin_lactose'][mod.id])*4.0

                    if 'EX_ac_e' in reac_mod.id:
                        reac_mod.lower_bound = 0.0
                        reac_mod.upper_bound= min(Y[param['Y_id']['EX_lcts_e']]/((param['limit_dt']*np.sum(Y[param['bact_Y_id_by_screened_compound']['EX_lcts_e']]))+epsilon),param['intrinsic_flux'][mod.id][reac.id])
                
                ######## freudenreichii ########
                elif mod.id in ['freudenreichii']:                    
                    if 'lac_' in reac_mod.id:                        
                        reac_mod.lower_bound = max(-Y[param['Y_id'][reac_mod.id]]/((param['limit_dt']*y_bact)+epsilon),param['intrinsic_flux_product'][reac_mod.id]* Y[param['Y_id']['freudenreichii']] / (10**param['thresh_qs_pure_culture']['freudenreichii']))
                    if 'lcts' in reac_mod.id:
                        reac_mod.lower_bound = max(-Y[param['Y_id'][reac_mod.id]]/((param['limit_dt']*y_bact)+epsilon),param['intrinsic_flux_product'][reac_mod.id]) # limiting substrate concentration dependent bound

                    if reac_mod.id in param['intrinsic_flux_product']['products']:
                        reac_mod.lower_bound=0
                        if reac_mod.id in ['EX_ac_e','EX_ppa_e','EX_succ_e']:
                            reac_mod.upper_bound=param['intrinsic_flux_product'][reac_mod.id] * Y[param['Y_id']['freudenreichii']] / (10**param['thresh_qs_pure_culture']['freudenreichii'])
                        else:
                            reac_mod.upper_bound=param['intrinsic_flux_product'][reac_mod.id]



        #### Internal reactions of the metabolic models to ensure physiological behaviour (cf material and method of Lecomte et al.)  ####
        #### A dynamic bound is added to internal reactions to avoid infeasible model in case of substrate depletion.                 ####
        #### When the main substrate is depleted (lactose), the internal reaction lower bound is modified according to availability.  ####
        constrained_substrate = ['EX_lcts_e']
            
        for r in param['spe_reac'][mod.id]:
            curr_cons = np.sum(np.array([Y[param['Y_id'][r_c]] for r_c in constrained_substrate]))
            curr_bact_id = np.array(list(set([b for r_c in constrained_substrate for b in param['bact_Y_id_by_screened_compound'][r_c]])))
            curr_reac=mod.reactions.get_by_id(r)    
            curr_reac.lower_bound = min(
                                     curr_cons/((param['limit_dt']*np.sum(Y[curr_bact_id]))+epsilon),
                                     param['spe_reac'][mod.id][r]['lower_bound']
                                     )







        ############## FBA computation #################
        ################################################
        try:
            if param['Parsimonious']:    
                pfba_sol = cobra.flux_analysis.pfba(mod)
            elif param['Lexicographic']:
            
            

                # Create an empty object to encapsulate the results in a compatible way with the following
                pfba_sol = Mock_sol() 

                ## Resolution method taken from https://cobrapy.readthedocs.io/en/latest/dfba.html and “DFBAlab: A Fast and Reliable MATLAB Code for Dynamic Flux Balance Analysis.” BMC Bioinformatics 15, no. 1 (December 18, 2014): 409. https://doi.org/10.1186/s12859-014-0409-8.
                if param['Lexicographic_Secured_Mode']:  
                    ## Check if the model is feasible given the boundaries and set a new objective function whose result is 0 if feasible, and non-null if infeasible
                    cobra.util.add_lp_feasibility(mod)
                    ## enforce the model to keep the feasibility status
                    feasibility = cobra.util.fix_objective_as_constraint(mod)                
                else:
                    feasibility=0.0
                    
                
                pfba_sol.fluxes = cobra.util.add_lexicographic_constraints(mod,param['Lexicographic_dict'][mod.id]['reaction_order'],param['Lexicographic_dict'][mod.id]['Direction'])
                
                if FLUX_OUTPUT:
                    if com:
                        flux_output = 'results/total_flux_'+mod.id+'_t_'+str(t)+'.pkz'
                    else:
                        flux_output = 'results/pure_culture_total_flux_'+mod.id+'_t_'+str(t)+'.pkz'
                    with mod as mod_save:
                             
                        for reac in [mod_save.reactions.get_by_id(r) for r in param['Lexicographic_dict'][mod.id]['reaction_order'][:-1]]:
                            reac.lower_bound=pfba_sol.fluxes[reac.id]
                            reac.upper_bound = pfba_sol.fluxes[reac.id]

                        reac = mod_save.reactions.get_by_id(param['Lexicographic_dict'][mod.id]['reaction_order'][-1])
                        mod_save.objective = reac.id
                        curr_res = mod_save.optimize()
                        with gzip.open(flux_output,'wb') as f:
                            pickle.dump({'result_'+mod.id+'_t_'+str(t):curr_res},f)                
                




                if feasibility<=1e-14:
                    pfba_sol.status='optimal'

                else:
                    pfba_sol.status='infeasible'
                    print("feasibility",feasibility)
            else:
                pfba_sol = mod.optimize()
                

            if not pfba_sol.status=='optimal':
                flag_err=True
                print("**********************************\n infeasible in "+bact_id, pfba_sol.status, [[mod.reactions.get_by_id(reac.id).lower_bound,mod.reactions.get_by_id(reac.id).upper_bound,-Y[param['Y_id'][reac.id]]/(param['limit_dt']*(y_bact)),reac.id] for reac in param['Screened_Compounds'][mod.id]])
                exit()
        except:       
            flag_err=True


            raise NameError('Infeasible model in metabolic model :',mod.id)    
        #################### From individual flux to population flux => scalling with bacterial population levels ################
        ##########################################################################################################################


        if not flag_err:            
            for reac in param['Screened_Compounds'][mod.id]:
                  
                if reac.id in mod.reactions and reac.id in pfba_sol.fluxes:
                    if mod.id in ['lactis','plantarum']:
                        optimal_flux[param['Y_id'][reac.id]]=pfba_sol.fluxes[reac.id]*Y[param['Y_id'][bact_id]]
      
                    elif mod.id in ['freudenreichii']:
                        if com:
                            if t < times.loc['Moul'][0]:
                                optimal_flux[param['Y_id'][reac.id]] = 0
                            else: 
                                optimal_flux[param['Y_id'][reac.id]]=pfba_sol.fluxes[reac.id]*Y[param['Y_id'][bact_id]]
                        else:
                            optimal_flux[param['Y_id'][reac.id]]=pfba_sol.fluxes[reac.id]*Y[param['Y_id'][bact_id]]
                            
                        current_bounds[param['Y_id'][reac.id]]=pfba_sol.fluxes[reac.id]

            if mod.id in ['lactis','plantarum']:
                optimal_flux[param['Y_id'][mod.id]]=pfba_sol.fluxes[param['biomass_function_dict'][mod.id]]*Y[param['Y_id'][mod.id]]

            elif mod.id in ['freudenreichii']:
        
                if com:
                    if t < times.loc['Moul'][0]:
                        optimal_flux[param['Y_id'][mod.id]]=0
                    else:
                        optimal_flux[param['Y_id'][mod.id]]=pfba_sol.fluxes[param['biomass_function_dict'][mod.id]]*Y[param['Y_id'][mod.id]]
                else:
                    optimal_flux[param['Y_id'][mod.id]]=pfba_sol.fluxes[param['biomass_function_dict'][mod.id]]*Y[param['Y_id'][mod.id]]
                current_bounds[param['Y_id'][mod.id]]=pfba_sol.fluxes[param['biomass_function_dict'][mod.id]]
    return optimal_flux,current_bounds,flag_err

def quorumSensing(Y,t,param,bact_id=''):
    """ Regulate the total population from a predefined threshold

    Args:
        Y (ndarray): Screening compounds
        t (float): current time
        param (dict): dictionnary of all essential parameters used in the dFBA and optim program
        bact_id (str, optional): id of the current bacterium. Defaults to ''.

    Returns:
        int: multiplicator term to regulate the fluxes
    """    
    epsilon=1e-12
    beta = 10**param['thresh_qs'][bact_id]
    if bact_id in ['plantarum','freudenreichii']:
        lambda_param  = param['lambda'][bact_id]
    else:
        lambda_param=1.0
    return lambda_param*max(0,1-((Y[param['Y_id'][bact_id]])/(beta+epsilon)))



def getUndissociatedAcidLactiqueConcentration(concentration_D,concentration_L,c1,c2,lactic_acid='total',Ka = 1.38 * 10**(-4)):
    epsilon=1e-9
    total_concentration = concentration_D + concentration_L + epsilon
    if lactic_acid == 'total':
        output = total_concentration/(1+10**(c1*total_concentration+c2))
    elif lactic_acid == 'dissociated':
        output = total_concentration/(10**(c1*(total_concentration*(1 + total_concentration/Ka))+c2))
    return output

def getLactatePh(concentration_D,concentration_L, c1=None , c2=None, lactic_acid = 'total', pKa = 3.86,Ka=1.38 * 10**(-4)):

    """
    Compute the global ph from lactate isomere concentration. 
    
    A correspondance between total lactate (HPLC) and pH has been fitted on community data with the relation pH = pKA + C1*[lac] + C2. 
    Here, the pH formula is given according to the modelled lactic acid:
        * if lactate represents total lactic acid, then
            pH = pKa+ C1*[lac] + C2
        * if lactate represents the dissociated lactic acid, then
            pH = pKa + C1*[lac](1+[lac]/Ka)+C2

    Args:
        concentration (float): contration computed by the dFBA in mmol.g-1

    Returns:
        float: the corresponding ph at a time step
    """    
    epsilon=1e-9    
    
    lac= (concentration_D+concentration_L)
    if lactic_acid == 'total':
        pH = pKa+c1*lac + c2
    elif lactic_acid == 'dissociated':
        
        pH = pKa+c1*lac*(lac/Ka) + c2
    return pH

def convert_g_kg_in_mmol_g(initial_concentration,molar_mass=342.3):
    """ For metabolite m with concentration M, the mass conservation equation reads 
     \[ d_t M = \mu_{FBA}(M,b) b \]
     where $\mu_{FBA}$ is the FBA model, scaled in $mmol.h^{-1}.g_{biomass weight}^{-1}$, and $b$ is the biomass concentration, scaled in $g_{biomass weight}.g_{milk}^{-1}$. Metabolite concentrations are then scaled in mmol.g_{milk}^{-1}. But data are given in g.kg_{milk}^{-1}. A conversion must be applied.
     
     We write : M [g.kg_{milk}^{-1}] / molar_mass [g.mol^{-1}] = M [mol.kg_{milk}^{-1}] = M [mmol.g_{milk}^{-1}]
    """
    return initial_concentration/molar_mass

def getLactateConcentrationFromPh(ph,c1=None,c2=None,pKa=3.86,Ka=1.38 * 10**(-4), lactic_acid = 'total'):
    """
    Inverse function of GetLactatePh concentration

    Here, the pH formula is given according to the modelled lactic acid:
        * if lactate represents total lactic acid, then
            [lac] = (pH - pKa - c2)/c1
        * if lactate represents the dissociated lactic acid, then
            [lac] = (-ka + sqrt(ka^2 - 4 Ka*(pKa + c2 - pH)/c1))/2
            (it is the positive root of [lac]^2 + Ka [lac] + Ka (-pH+pKa+c2)/c1)
    """

    if lactic_acid == 'total':
        lac_conc = np.maximum((ph - pKa - c2)/c1,0)
    elif lactic_acid == 'dissociated':
        lac_conc = (-Ka + np.sqrt(np.maximum(Ka**2 - 4*Ka*(pKa + c2 - ph)/c1,Ka**2)))/2
    return lac_conc

def ElementaryPlot(axes,dfba_prediction,data=None,label=''):
    if data is not None:      
        if label=='pH':
            try:
                axes.scatter(data['mean']['time'],data['mean'][label],label='Experimental growth',marker='s',s=5,color='b')        
                y_est_min = np.array(data['mean'][label])-np.sqrt(2)*np.array(data['std'][label])
                y_est_max = np.array(data['mean'][label])+np.sqrt(2)*np.array(data['std'][label])
                axes.plot(dfba_prediction['time'],dfba_prediction[label],label='Simulated Growth',color='b')
            except:
                axes.scatter(data['mean']['time'],data['mean'][label.lower()],label='Experimental growth',marker='s',s=5,color='b')        
                y_est_min = np.array(data['mean'][label.lower()])-np.sqrt(2)*np.array(data['std'][label.lower()])
                y_est_max = np.array(data['mean'][label.lower()])+np.sqrt(2)*np.array(data['std'][label.lower()])
                axes.plot(dfba_prediction['time'],dfba_prediction[label],label='Simulated Growth',color='b')
        else:
            axes.scatter(data['mean']['time'],10**data['mean'][label],label='Experimental growth',marker='s',s=5,color='b')        
            y_est_max = 10**(np.sqrt(2)*np.array(data['std'][label]))*np.array(10**data['mean'][label])
            y_est_min = 10**(-np.sqrt(2)*np.array(data['std'][label]))*np.array(10**data['mean'][label])
            axes.plot(dfba_prediction['time'],dfba_prediction[label],label='Simulated Growth',color='b')
        axes.fill_between(data['mean']['time'],y_est_min,y_est_max, facecolor = 'black',alpha=0.2,label='Experimental error') ## 1/2 log range
    
    
def PlotDFBAResult(param,model,dfba_prediction=None,data=None,label='numbering',tag=''):
    if data is None:
        curr_data = {"ExpGrowth":None}
    else:
        curr_data=data

    fig,axes = plt.subplots()
    try:        
        ElementaryPlot(axes,dfba_prediction['ExpGrowth'],data=curr_data['ExpGrowth'],label=label)
    except:
        ElementaryPlot(axes,dfba_prediction['ExpMetabolites'],data=curr_data['ExpMetabolites'],label=model.id)
    axes.set_yscale("log")
    axes.set_ylabel("g_DW.g-1 of milk")
    axes.set_xlabel("time in hours")
    axes.legend(loc='lower right')   
    fig.savefig('pipeline/dfba_trajectory_'+str(model.id)+'_'+'_'.join(label.split(' '))+'_'+tag+'.png',bbox_inches='tight')
    ## ph part
    if model.id != 'freudenreichii':
        fig,axes = plt.subplots()
        ElementaryPlot(axes,dfba_prediction['ExpGrowth'],data=curr_data['ExpGrowth'],label='pH')
        axes.set_ylabel("ph")
        axes.set_xlabel("time in hours")
        axes.ticklabel_format(useOffset=False)
        axes.legend()            
        fig.savefig('pipeline/dfba_trajectory_ph_'+str(model.id)+tag+'.png',bbox_inches='tight')
    else:
        fig,axes = plt.subplots()
        try:
            d= data['ExpMetabolites']['mean'].drop(['time',model.id],axis=1)
            d_pred=dfba_prediction['ExpMetabolites'].drop(['time',model.id],axis=1)
            d_std = data['ExpMetabolites']['std'].drop(['time',model.id],axis=1)
        except:
            d= data['ExpMetabolites']['mean'].drop(['time'],axis=1)
            d_pred=dfba_prediction['ExpMetabolites'].drop(['time'],axis=1)
            d_std = data['ExpMetabolites']['std'].drop(['time'],axis=1)
        d_val_index=curr_data['ExpMetabolites']['mean']['time']==np.max(curr_data['ExpMetabolites']['mean']['time'])
        axes.scatter(d.columns,d[d_val_index],label='data',marker='o',color='gray',s=5,zorder=1)
        axes.errorbar(d.columns,d[d_val_index].values.ravel(),yerr=d_std[d_val_index].values.ravel(),fmt='.',ecolor="gray",capsize=5,color='gray',zorder=1) 
        axes.scatter(d.columns,d_pred[d_val_index],label='dfba prediction',marker='o',color='blue',s=5,zorder=2)            
        axes.set_ylabel("concentration")
        axes.set_xlabel("metabolites")
        axes.legend()
            
        fig.savefig('pipeline/dfba_trajectory_metabolite_'+str(model.id)+'_'+'_'.join(label.split(' '))+'_'+tag+'.png',bbox_inches='tight')
    plt.close()


        
        
