import argparse
import csv
import json
import os
import sys
import time

import numpy as np
import pandas as pd

from . import customize, dFBA, optim, preprocessing_data, simulation, species


def main(argv=[]):
    parser_c=argparse.ArgumentParser()

    ############## Sim ###############
    parent_parser_model_path=argparse.ArgumentParser(add_help=False)
    parent_parser_model_path.add_argument("-mp", "--model_path",nargs='+',help='path of each sbml model. if list of path, please separate them with a blank space. ex : path1 path2 path3')

    parent_parser_sim_dfba_culture=argparse.ArgumentParser(add_help=False)
    parent_parser_sim_dfba_culture.add_argument("-cp", "--culture_path", default='pipeline/config_file/config_culture.yml',help='Path of specific media and/or specif modification applied to the FBA models')
    parent_parser_sim_dfba_culture.add_argument("-CobraSolver", "--Cobra_Solver",help='Choose the FBA solver used for FBA computation by Cobra.', type=str, default='cplex', choices={'cplex','glpk_exact', 'glpk', 'scipy'})

    parent_parser_sim_dfba_dynamic=argparse.ArgumentParser(add_help=False)
    parent_parser_sim_dfba_dynamic.add_argument("-dp", "--dynamic_path", default='pipeline/config_file/config_dynamic.yml',help='Path of specific paramters for your dFBA analysis')

    parent_parser_sim_optim=argparse.ArgumentParser(add_help=False)
    parent_parser_sim_optim.add_argument("-sp", "--solver_path", default='pipeline/config_file/config_optim.yml', help='Path of the configuration file for optimization')

    parent_parser_sim_com=argparse.ArgumentParser(add_help=False)
    parent_parser_sim_com.add_argument("-com", "--com", type=str, choices={"True","False"} ,help='dFBA at the community scale')

    parent_parser_sim_opt=argparse.ArgumentParser(add_help=False)
    parent_parser_sim_opt.add_argument("-optim", "--optim", type=str, default='False', choices={"True","False"}, help='active or not the optimization on parameters')
    parent_parser_sim_opt.add_argument("-r", "--recovery", type=str, default='False', choices={"True","False"}, help='active or not the optimization on parameters')    
    parent_parser_sim_opt.add_argument("-v", "--verbose", type=str, default='False', choices={"True","False"}, help='active or not the verbose option')
    parent_parser_sim_opt.add_argument("-lam", "--lactic_acid_model", type=str, default='total', choices={"total","dissociated"}, help='lactic acid model : total if lactate represents total lactic acid concentration, or dissociated if lactate represents the dissociated lactic acid')
    parent_parser_sim_opt.add_argument("-fsim", "--freud_sim", type=str, default='growth', choices={"growth","metabolites"}, help='Different initial conditions for lactate where defined in the experiments for growth and metabolite dosage => this parameter allows to switch between both situations')

    ######################################### subparser parser #######################################
    subparsers = parser_c.add_subparsers(title='command',description='valid subcommands:',dest="cmd")

    config_subparser_s=subparsers.add_parser("sim",help="start the simulation",parents=[parent_parser_model_path,parent_parser_sim_dfba_culture,parent_parser_sim_dfba_dynamic,parent_parser_sim_optim,parent_parser_sim_com,parent_parser_sim_opt])

    args_c = parser_c.parse_args()


    list_model=species.load(args_c.model_path)

    print('simulation part')
    culture_path=args_c.culture_path
    dynamic_path=args_c.dynamic_path
    solver_path=args_c.solver_path
    args_c.verbose=args_c.verbose=='True'

    sp_reac,media_reac,param_sp = simulation.get_config_from_file(culture_config=culture_path,dynamic_config=dynamic_path,optim_config=solver_path)
    simulation.sim(sp_reac,media_reac,param_sp,list_model,args_c)



    if args_c.com == 'True':
        with open('pipeline/commandline_args_com.json', 'w+') as f:
            json.dump([args_c.__dict__,param_sp,media_reac,sp_reac], f, indent=2)
    else:
        with open('pipeline/commandline_args.json', 'w+') as f:		
            json.dump([args_c.__dict__,param_sp,media_reac,sp_reac], f, indent=2)


#----- If called as script, call main program -------------

if __name__ == '__main__':
    main(sys.argv[1:])
