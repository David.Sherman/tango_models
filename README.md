# Tango

TANGO uses a numerical-based strategy to reconcile multi-omics data and metabolic networks for characterising bacterial fermentation in cheese production composed of 3 species : *P. freudenreichii*, *L. lactis* and *L. plantarum*. 

Please read the [research paper](https://hal.inrae.fr/hal-04088301v2) for details about the models and the global modeling strategy. 

## Install

To build the `tango_models` package, use `hatch`:

```sh
hatch build
```

The TANGO program has to be executed in a specific Conda virtual environment. To install Conda on your computer, please visit [the Conda installation webpage](https://conda.io/projects/conda/en/latest/user-guide/install/index.html). Once installed, in a terminal, run

```sh
conda env create -f environment.yml 
```

To install the `tango_models` program in this environment, use `pip`:

```sh
pip install dist/tango*.whl
```


## Overall strategy of the Tango models

- Get metabolic models of the 3 species, and refine them to be consistent with biological knowledge. The refined models are provided  in the folder 'metabolic_models'.
- Calibrate all individual model based on growth, pH and dosage time series obtained on individual growth culture in milk. 
- Assemble individual model into a community model, the output of which can be compared to community growth culture.


## Run simulations 

Simulations can be run with the command line interface.

When running manually, the `results` directory must exist
```sh
mkdir -p results
```


### Running individual model optimization 

By setting the option -optim to True, an optimization is launched. The optimization parameters can be set in the configuration file in 'pipeline/config_file/config_optim.yml'. Here, the freudenreichii model is optimized. By changing the model name in the command line, the model used for optimization can be changed.

```
python -m src.tango_models sim -mp metabolic_models/freudenreichii.sbml -cp pipeline/config_file/config_culture.yml -dp pipeline/config_file/config_dynamic.yml -sp pipeline/config_file/config_optim.yml -com False -optim True -CobraSolver glpk
```

### Running individual model

An individual model is launched by setting the option -optim to False. The individual model dynamical parameters can be set in the configuration file in 'pipeline/config_file/config_dynamic.yml'. Common parameter to individual and culture growth experiments are defined in the 'pipeline/config_file/config_culture.yml'. By changing the model name in the command line, the model used for the simulation can be changed. 

```
python -m src.tango_models sim -mp metabolic_models/freudenreichii.sbml -cp pipeline/config_file/config_culture.yml -dp pipeline/config_file/config_dynamic.yml -com False -optim False -CobraSolver glpk

python -m src.tango_models sim -mp metabolic_models/lactis.sbml -cp pipeline/config_file/config_culture.yml -dp pipeline/config_file/config_dynamic.yml -com False -optim False -CobraSolver glpk

python -m src.tango_models sim -mp metabolic_models/plantarum.sbml -cp pipeline/config_file/config_culture.yml -dp pipeline/config_file/config_dynamic.yml -com False -optim False -CobraSolver glpk
```

For Freudenreichii, another experiment with different initial condition has been done for metabolite dosage. This experiment can be model with the optiono ```-fsim```.
```
python -m src.tango_models sim -mp metabolic_models/freudenreichii.sbml -cp pipeline/config_file/config_culture.yml -dp pipeline/config_file/config_dynamic.yml -com False -optim False -CobraSolver glpk -fsim metabolites
```

### Running community models

For running dFBA in community, set ```-com True```, and specify the list of models in the community. The community model dynamical parameters can be set in the configuration file in 'pipeline/config_file/config_dynamic_com.yml'.

```
python -m src.tango_models sim -mp metabolic_models/freudenreichii.sbml metabolic_models/lactis.sbml metabolic_models/plantarum.sbml -cp pipeline/config_file/config_culture.yml -dp pipeline/config_file/config_dynamic_com.yml -sp pipeline/config_file/config_optim.yml -com True -optim False -CobraSolver glpk
```


## Plot simulations

Several plotting tools are available.

```
python -m scripts.plot_indiv
python -m scripts.plot_com
python -m scripts.plot_flux
python -m scripts.plot_goodness_of_fit
python -m scripts.plot_switch_pathways
python -m scripts.plot_transcripts
```


## Run using CWL

The complete [Common Workflow Language workflow](https://www.commonwl.org/user_guide/topics/workflows.html)
for TANGO and a job file defining its experimental setup can be executed using 

```sh
cwltool --verbose --no-container tango_workflow.cwl tango_experiments_job.yml
```

Summary outputs are copied into `results`, and figures for the
manuscript are copied into `figures`.

The directory `pipeline` contains a CWL [CommandLineTool](https://www.commonwl.org/user_guide/topics/command-line-tool.html) for `tango_models`, as well as CWL [Workflows](https://www.commonwl.org/user_guide/topics/workflows.html) for pipeline steps.
Individual pipeline steps can be executed:

```sh
cwltool --no-container pipeline/tango_optimization_wf.cwl tango_experiments_job.yml
cwltool --no-container pipeline/tango_dynamics_wf.cwl tango_experiments_job.yml
cwltool --no-container pipeline/tango_plots.cwl tango_experiments_job.yml
```

The CWL specification assumes that software requirements are already met,
so the Conda environment must be activated, and TANGO installed.
If a CWL runner is not already installed on your system, add it using
`conda install cwltool`.

### Record provenance in a RO-Crate

The `cwltool` reference implementation can record result provenance in a
[Research Object Crate](https://www.researchobject.org/ro-crate/):

```sh
cwltool --no-container --provenance ro-crate tango_workflow.cwl tango_experiments_job.yml
```


## How to cite

- TANGO:<br/>
    Lecomte M, Cao W, Aubert J, Sherman DJ, Falentin H, Frioux C, Labarthe S. (2024) _Revealing the dynamics and mechanisms of bacterial interactions in cheese production with metabolic modelling._
    **Metab Eng**. 83:24-38.
    doi: https://doi.org/10.1016/j.ymben.2024.02.014.
- Growth and pH data used in TANGO:<br/>
    Cao W, Aubert J, Maillard MB, Boissel F, Leduc A, Thomas JL, Deutsch SM, Camier B, Kerjouh A, Parayre S, Harel-Oger M, Garric G, Thierry A, Falentin H. (2021) _Fine-Tuning of Process Parameters Modulates Specific Metabolic Bacterial Activities and Aroma Compound Production in Semi-Hard Cheese._
    **J Agric Food Chem**. 69(30):8511-8529.
    doi: https://doi.org/10.1021/acs.jafc.1c01634.
- Genomic data for bacterial strains:<br/>
    [ENA: PRJEB54980](https://www.ebi.ac.uk/ena/browser/view/PRJEB54980)
- Metatranscriptomic data:<br/>
    [ENA: PRJEB42478](https://www.ebi.ac.uk/ena/browser/view/PRJEB42478)
- Annotated genomes, RNA reads and RPKM:<br/>
    https://entrepot.recherche.data.gouv.fr/privateurl.xhtml?token=4bf01466-29dd-4d71-b25e-b775d9cc39dc
