## Authors

- Julie Aubert
    - https://orcid.org/0000-0001-5203-5748
    - mailto:julie.aubert@inrae.fr
    - [MIA Paris-Saclay](https://rnsr.adc.education.fr/structure/201119642G) - 
      Mathématiques et Informatique Appliquées - AgroParisTech,
      Université Paris-Saclay, INRAE
- Hélène Falentin
    - https://orcid.org/0000-0001-6254-5303
    - mailto:helene.falentin@inrae.fr
    - [STLO](https://rnsr.adc.education.fr/structure/199117840K) -
      Science et Technologie du Lait et de l'Oeuf - INRAE, Institut
      Agro Rennes Angers
- Clémence Frioux
    - https://orcid.org/0000-0003-2114-0697
    - mailto:clemence.frioux@inria.fr
    - [PLEIADE](https://rnsr.adc.education.fr/structure/201521167X) -
      Pleiade, from patterns to models in computational biodiversity
      and biotechnology - Inria, INRAE, Université de Bordeaux
- Simon Labarthe
    - https://orcid.org/0000-0002-5463-7256
    - mailto:simon.labarthe@inrae.fr
    - [BioGeCo](https://rnsr.adc.education.fr/structure/200317684N) -
      Biodiversité, Gènes & Communautés - Université de Bordeaux, INRAE
    - [PLEIADE](https://rnsr.adc.education.fr/structure/201521167X) -
      Pleiade, from patterns to models in computational biodiversity
      and biotechnology - Inria, INRAE, Université de Bordeaux
- Maxime Lecomte
    - https://orcid.org/0000-0002-4558-6151
    - mailto:maxime.lecomte@inria.fr
    - [STLO](https://rnsr.adc.education.fr/structure/199117840K) -
      Science et Technologie du Lait et de l'Oeuf - INRAE, Institut
      Agro Rennes Angers
    - [PLEIADE](https://rnsr.adc.education.fr/structure/201521167X) -
      Pleiade, from patterns to models in computational biodiversity
      and biotechnology - Inria, INRAE, Université de Bordeaux
- David James Sherman
    - https://orcid.org/0000-0002-2316-1005
    - mailto:david.sherman@inria.fr
    - [PLEIADE](https://rnsr.adc.education.fr/structure/201521167X) -
      Pleiade, from patterns to models in computational biodiversity
      and biotechnology - Inria, INRAE, Université de Bordeaux

## Acknowledgements

The authors acknowledge the Galaxy Genotoul bioinformatics platform
(Toulouse, France), the MicroScope platform hosted at Genoscope (CEA,
Evry, France), and the GenOuest bioinformatics core facility
(https://www.genouest.org) for providing the computing
infrastructure. CF, DJS and SL were supported by the French National
Research Agency (ANR) France 2030 PEPR Agroécologie Numérique MISTIC
ANR-22-PEAE-0011. CF and SL were supported by the Inria Exploratory
Action SLIMMEST. We are grateful to the INRAE STLO Dairy Platform
(https://www6.rennes.inrae.fr/plateforme_lait_eng/,
https://doi.org/10.15454/1.557240525786479E12) for providing help and
support, and to the CNIEL for partial funding and fruitful discussions
on the interpretation of the results (TANGO Project).
