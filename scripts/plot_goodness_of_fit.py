import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy.stats as scps
from .plot_com import get_data_com
from .plot_indiv import get_data

fields_com = ['results_w', 'experiment_time_series', 'simulation', 'bact', 'absolute', 'time', 'time_o']
fields_indiv = ['results_w', 'Conc', 'Growth', 'bact', 'd_type']
data_indiv = get_data()

data_com = get_data_com()

data={'com':{i:data_com[i_i] for i_i,i in enumerate(fields_com)},'indiv':{i:data_indiv[i_i] for i_i,i in enumerate(fields_indiv)}}
data['com']['simulation']['EX_lac__L_e']=data['com']['simulation']['EX_lac__L_e']+data['com']['simulation']['EX_lac__D_e']
y={"com":[],"indiv":[]}
yhat={"com":[],"indiv":[]}
rsquare={"com":[],'indiv':[]}
rsquare_label={"com":[],'indiv':[]}
outlier ={"com":[],"indiv":[]}
eps=1e-9
# Co-culture data
for c in data['com']['simulation'].columns:
    if c!="time" and c!='EX_diact_e':
        if c in data['com']['experiment_time_series'].keys():
            index = data['com']['experiment_time_series'][c]['mean'].index[data['com']['experiment_time_series'][c]['mean'].notna()]
            curr_y = data['com']['experiment_time_series'][c]['mean'].loc[index]
            curr_y[curr_y==0]=eps
            curr_hat_y = data['com']['simulation'][c].loc[index]
            y['com'].append(curr_y)
            yhat['com'].append(curr_hat_y)
            rsquare['com'].append(1 - np.sum((curr_y-curr_hat_y)**2)/np.sum((curr_y - np.mean(curr_y))**2))
            rsquare_label['com'].append(c)
            outlier['com'].append(index[np.log10(curr_y)-np.log10(curr_hat_y)>4])

#Mono-culture data
for b in data['indiv']['bact']:
    curr_y = data['indiv']['Growth'][b]['numbering']['mean']
    y['indiv'].append(curr_y)
    index = [float(i) for i in data['indiv']['Growth'][b]['numbering']['mean'].index]
    if b != 'freudenreichii':
        curr_hat_y = data['indiv']['results_w'][b][b].loc[index]    
    else:
        curr_hat_y = data['indiv']['results_w'][b+'_growth'][b].loc[index]
    yhat['indiv'].append(curr_hat_y)
    rsquare['indiv'].append(1 - np.sum((curr_y-curr_hat_y)**2)/np.sum((curr_y - np.mean(curr_y))**2))
    rsquare_label['indiv'].append(b)
    outlier['indiv'].append([])

for b in ['lactis','plantarum']:
    curr_y = data['indiv']['Growth'][b]['pH']['mean']
    y['indiv'].append(curr_y)
    index = [float(i) for i in data['indiv']['Growth'][b]['pH']['mean'].index]
    curr_hat_y = data['indiv']['results_w'][b]['ph'].loc[index]
    yhat['indiv'].append(curr_hat_y)
    rsquare['indiv'].append(1 - np.sum((curr_y-curr_hat_y)**2)/np.sum((curr_y - np.mean(curr_y))**2))
    rsquare_label['indiv'].append(b+' - pH')    
    outlier['indiv'].append([])


metabolites = data['indiv']['Conc']['mean'].iloc[-1]
curr_y = metabolites
y['indiv'].append(curr_y)
curr_hat_y = data['indiv']['results_w']['freudenreichii_metabolites'][metabolites.index].loc[data['indiv']['Conc']['mean'].index[-1]]
yhat['indiv'].append(curr_hat_y)
rsquare['indiv'].append(1 - np.sum((curr_y-curr_hat_y)**2)/np.sum((curr_y - np.mean(curr_y))**2))
rsquare_label['indiv'].append('metabolites')

res = {}
rsquare_v={}
rsquare_w={}
y_v={}
yhat_v={}
for dtype in ['indiv','com']:
    y_v[dtype] = pd.concat(y[dtype]).values
    yhat_v[dtype] = pd.concat(yhat[dtype]).values
    res[dtype]=scps.linregress(y_v[dtype],yhat_v[dtype])
    rsquare_v[dtype] = np.mean(np.array(rsquare[dtype]))
    rsquare_w[dtype] =1 - np.sum((y_v[dtype]-yhat_v[dtype])**2)/np.sum((y_v[dtype] - np.mean(y_v[dtype]))**2)



##### Figure

## General format
mm = 1.0/25.4 # mm to inches
width = 175.0 #mm
aspect_ratio=1.0

ncols=2
square_size= width*mm/ncols
    
SMALL_SIZE = 5
MEDIUM_SIZE = 6
BIGGER_SIZE = 7
font={'small':SMALL_SIZE,'medium':MEDIUM_SIZE,'big':BIGGER_SIZE}
plt.rc('font', family='Arial',size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes',titlesize=MEDIUM_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=SMALL_SIZE)    # fontsize of the x and y labels
plt.rc('xtick',labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=MEDIUM_SIZE)  # fontsize of the figure title
    
hatch=''

Title = {'train':'indiv','test':'com'}

color_c={dtype:{label:'C'+str(i_l+i_d*len(rsquare_label[dtype])) for i_l, label in enumerate(rsquare_label[dtype])} for i_d,dtype in enumerate(['indiv','com'])}
map_time = {
                'Linoc':'Linoc',
                'Finoc':'Finoc',
                'Moul':'Cm',
                'Demoul':'Cdm',
                'AS':'C0w',
                'Aff4s':'C4w',
                'Aff7s':'C7w'
                }



fig, ax = plt.subplots(1,ncols,figsize=((ncols*square_size,aspect_ratio*square_size)),constrained_layout=True)

shift=5
for i_d,dtype in enumerate(Title.items()):
    for i_l,label in enumerate(rsquare_label[dtype[1]]):
        ax[i_d].scatter(yhat[dtype[1]][i_l],y[dtype[1]][i_l],s=12,c=color_c[dtype[1]][label],label=label,alpha=0.8,edgecolor='none')
        if dtype[1]=="com":
            for i_out,outl in enumerate(outlier['com'][i_l]):
                ax[i_d].annotate(map_time[outl],
                                 xy=(yhat[dtype[1]][i_l].loc[outl],y[dtype[1]][i_l].loc[outl]),
                                 xytext=(3,-i_out), textcoords='offset points',
                                 va='center')
                                 #arrowprops=dict(arrowstyle="-"))
    x_range= np.array([yhat_v[dtype[1]].min(),yhat_v[dtype[1]].max()])
    #arange = np.logspace(x_range[0],x_range[1])
    ax[i_d].plot(x_range,x_range,c='r', ls=':',lw=0.5)
    #arange = np.sort(y_v[dtype[1]])
    #ax[i_d].plot(arange,res[dtype[1]].intercept + res[dtype[1]].slope * arange,c='g',ls=':')
     
#    print(x_range,res[dtype[1]].intercept + res[dtype[1]].slope * x_range )
    #ax[i_d].text(0.05,0.95,'slope: '+str(np.floor(1000*res[dtype[1]].slope)/1000),transform=ax[i_d].transAxes)
    ax[i_d].text(0.05,0.95,r'$R^2$: '+str(np.floor(100*rsquare_w[dtype[1]])/100),transform=ax[i_d].transAxes)    
    ax[i_d].set_title(dtype[0])
    ax[i_d].set_ylabel('y')
    ax[i_d].set_xlabel(r'$\hat{y}$')   
    ax[i_d].set_yscale('log')
    ax[i_d].set_xscale('log')
    ax[i_d].legend(loc='lower right')

fig.show()
fig.savefig('qqplot.pdf')

