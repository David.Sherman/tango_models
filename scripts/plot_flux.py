import gzip
import pickle

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from tango_models import dFBA


def get_data_flux():

    with gzip.open('results/result_community_model_whole.pkz','rb') as f:
        A=pickle.load(f)
        param=A['param']
        results_w=A['result']
        results_w = pd.DataFrame(results_w,columns=[param['Y_output_id_inv'][i] for i in range(results_w.shape[1])])
        results_flux = A['flux']
        results_flux = {i : pd.DataFrame(results_flux[i],columns=[param['Y_output_id_inv'][i] for i in range(results_flux[i].shape[1])]) for i in results_flux.keys()}
    return results_w, results_flux, param


map_name={'lactis':r'$L.lactis$',
                'plantarum':r'$L.plantarum$',
                'freudenreichii':r'$P.freudenreichii$',
                'EX_gal_e':'Galactose', 
                'EX_lac__L_e':'Lactate', 
                'EX_lcts_e':'Lactose', 
                'EX_pyr_e':'Pyruvate',
                'EX_succ_e':'Succinate', 
                'EX_lac__D_e': 'Lactate',
                'EX_diact_e':'Diacetyl', 
                'EX_cit_e':'Citrate',
                'EX_ac_e':'Acetate',
                'EX_ppa_e':'Propionate', 
                'EX_btd_RR_e':'Butanediol',
                'ph':'pH'}
plot_order = ['EX_lcts_e','EX_cit_e', 'EX_lac__L_e', 'EX_ac_e', 'EX_btd_RR_e', 'EX_diact_e','EX_ppa_e','EX_succ_e']

results_w, results_flux, param  = get_data_flux()

for b in param['bact']:
    results_flux[b]['EX_lac__L_e'] =     results_flux[b]['EX_lac__L_e'] +     results_flux[b]['EX_lac__D_e']
results_w['EX_lac__L_e']=results_w['EX_lac__L_e']+results_w['EX_lac__D_e']

##### Figure

## General format
mm = 1.0/25.4 # mm to inches
width = 175.0 #mm
aspect_ratio=0.9

ncols=3
square_size= width*mm/ncols
    
SMALL_SIZE = 7
MEDIUM_SIZE = 8
BIGGER_SIZE = 9
font={'small':SMALL_SIZE,'medium':MEDIUM_SIZE,'big':BIGGER_SIZE}
plt.rc('font', family='Arial',size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes',titlesize=MEDIUM_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=SMALL_SIZE)    # fontsize of the x and y labels
plt.rc('xtick',labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=MEDIUM_SIZE)  # fontsize of the figure title
    
hatch=''

nraws=3

fig = plt.figure(figsize=((ncols*square_size,aspect_ratio*nraws*square_size)),constrained_layout=True)
    
gs = fig.add_gridspec(nraws+1,ncols,height_ratios=[2/9,2/9,2/9,1/3])

color_c={b:'C'+str(i_b) for i_b,b in enumerate(param['bact'])}
axs={}    

for i_met,met in enumerate(plot_order):
    i = i_met//nraws
    j = i_met % nraws
    axs[met] = fig.add_subplot(gs[i,j])
    axs[met].plot(results_flux[b]['time'],0*results_flux[b]['time'],ls='-',lw=0.1,c='k')
    for b in results_flux.keys():
        axs[met].plot(results_flux[b]['time'],results_flux[b][met],ls='-',lw=1,c=color_c[b])    
    curr_ax = axs[met].twinx()
    curr_ax.plot(results_w['time'],results_w[met],ls='--',lw=1.,c='k')
    axs[met].set_title(map_name[met])
    if i==2:
        axs[met].set_xlabel('time [h]')
    if j==0:
        axs[met].set_ylabel(r'scalled flux $[mmol.h^{-1}.L^{-1}]$')
    if i<=1:
        if j==2:        
            curr_ax.set_ylabel(r'density $[mmol.L^{-1}]$')
    else:
        if j==1:
            curr_ax.set_ylabel(r'density $[mmol.L^{-1}]$')
    if met == "EX_lac__L_e":
        axs[met].set_ylim(-0.0001,0.0002)
    axs[met].ticklabel_format(style='sci',axis='y', scilimits = (0,0))
fig.text(0.01, 0.99, '(a)', horizontalalignment='center',verticalalignment='center', transform=fig.transFigure,fontsize=font['big'],fontweight='bold')
fig.text(0.01, 0.31, '(b)', horizontalalignment='center',verticalalignment='center', transform=fig.transFigure,fontsize=font['big'],fontweight='bold')
axs["legend"] = fig.add_subplot(gs[-2,-1])
curr_axs = axs['legend'].twinx()
for b in results_flux.keys():
    axs["legend"].plot([0,1],[-1,-1],ls='-',lw=1,c=color_c[b],label=map_name[b])
curr_axs.plot([0,1],[-1,-1],ls='--',lw=1.,c='k',label='metabolite \n density')
axs['legend'].set_ylim(0,1)
axs['legend'].axis('off')
curr_axs.axis('off')
curr_axs.set_ylim(0,1)
axs['legend'].legend(loc='upper left')    
curr_axs.legend(loc='lower left')    



#fig = plt.figure(figsize=((ncols*square_size,aspect_ratio*nraws*square_size)),constrained_layout=True)
    
#gs = fig.add_gridspec(nraws,ncols)

#color_c={b:'C'+str(i_b) for i_b,b in enumerate(param['bact'])}
#axs={}    

#for i_met,met in enumerate([a for a in results_flux['lactis'].columns if 'EX_' in a and 'EX_lac__D' not in a]):
#    i = i_met//nraws
#    j = i_met % nraws
#    axs[met] = fig.add_subplot(gs[i,j])
#    curry=np.ones(results_flux[b]['time'].iloc[:-1].shape)*results_w[met].iloc[0]
#    for b in results_flux.keys():
#        curr_plot = np.cumsum((results_flux[b][met].values[1:]+results_flux[b][met].values[:-1])/2*(results_flux[b]['time'].values[1:]-results_flux[b]['time'].values[:-1]))
#        print(curr_plot.shape, curry.shape, results_flux[b]['time'].iloc[:-1].shape)    
#        axs[met].fill_between(results_flux[b]['time'].iloc[:-1],curry,curry+curr_plot,ls='-',lw=1,color=color_c[b])
#        curry+=curr_plot
#    axs[met].plot(results_w['time'],results_w[met],ls='-',lw=2,c='k')
#    axs[met].set_title(met)
#    if i==2:
#        axs[met].set_xlabel('time [h]')
#    if j==0:
#        axs[met].set_ylabel(r'density $[mmol.L^{-1}]$')

            
#fig.savefig('Community_flux_inside.pdf')


#fig = plt.figure(figsize=((2*square_size,square_size)),constrained_layout=True)
    
#gs = fig.add_gridspec(1,1)


axs=fig.add_subplot(gs[-1,:])


color_c={b:'C'+str(i_b) for i_b,b in enumerate(param['bact'])}


contribution={b:{met:0.0 for met in [a for a in results_flux['lactis'].columns if 'EX_' in a and 'EX_lac__D' not in a]} for b in param["bact"]}


for i_met,met in enumerate(plot_order):
    negative_y=0.0
    positive_y = 0.0
    for b in results_flux.keys():
        contribution[b][met] = np.cumsum((results_flux[b][met].values[1:]+results_flux[b][met].values[:-1])/2*(results_flux[b]['time'].values[1:]-results_flux[b]['time'].values[:-1]))
    normal = np.abs(np.sum(np.array([contribution[b][met][-1] for b in param['bact']])))
    #normal=1.0
    for b in results_flux.keys():
        x_met=i_met   
        if i_met ==0:
            kwargs={'label':map_name[b]}
        else:
            kwargs={}    
        if contribution[b][met][-1]<0:
            if met=='EX_lac__L_e':
                x_met+=0.15
            axs.bar(x_met,contribution[b][met][-1]/normal,width=0.3, bottom = negative_y,color= color_c[b],**kwargs)
            negative_y+=contribution[b][met][-1]/normal
        else:
            if met=='EX_lac__L_e':
                x_met-=0.15        
            axs.bar(x_met,contribution[b][met][-1]/normal,width=0.3, bottom = positive_y,color= color_c[b],**kwargs)
            positive_y+=contribution[b][met][-1]/normal
    print(met,[contribution[b][met][-1]/normal for b in results_flux.keys()])
axs.plot([-0.5,len(plot_order)-0.5],[1.0,1.0],lw=0.4,ls=':',c='k')
axs.plot([-0.5,len(plot_order)-0.5],[-1.0,-1.0],lw=0.4,ls=':',c='k')
axs.plot([-0.5,len(plot_order)-0.5],[.0,.0],lw=0.6,ls='-',c='k')
axs.fill_between([-0.5,len(plot_order)-0.5],[0,0],[1.5,1.5],color='g',alpha=0.1,zorder=-1,lw=0)
axs.fill_between([-0.5,len(plot_order)-0.5],[0,0],[-1.5,-1.5],color='r',alpha=0.1,zorder=-1,lw=0)
axs.text(0.5,0.1,'Consumption',horizontalalignment='center',verticalalignment='center',transform=axs.transAxes,fontsize=font['big'],fontweight='bold')
axs.text(0.5,0.9,'Production',horizontalalignment='center',verticalalignment='center',transform=axs.transAxes,fontsize=font['big'],fontweight='bold')
axs.set_xticks([i_a for i_a,_ in enumerate([a for a in results_flux['lactis'].columns if 'EX_' in a and 'EX_lac__D' not in a])])
axs.set_xticklabels([map_name[a] for a in plot_order])
axs.set_xlabel('Metabolites')
axs.set_ylabel('Relative contribution')    
axs.legend(loc='lower right')

fig.savefig('Community_flux_bact_contribution.pdf')




