import gzip
import math
import pickle

import matplotlib.pyplot as plt
import matplotlib.transforms as transforms
import numpy as np
import pandas as pd

from tango_models import dFBA


def get_data_com():

    with gzip.open('results/result_community_model_whole.pkz','rb') as f:
        A=pickle.load(f)
        param=A['param']
        results_w=A['result']
        results_w = pd.DataFrame(results_w,columns=[param['Y_output_id_inv'][i] for i in range(results_w.shape[1])])   

    simulation = pd.read_csv('results/result_dfba_com_without_optim.csv',sep='\t').set_index('State')
    time_o = pd.read_csv('data/com_time.tsv',sep='\t').set_index('State')
    time = time_o.loc[simulation.index]


    experiments = pd.read_excel('data/TANGO_MTA_WF_310320.xlsx').set_index('Stade')



    experiments = experiments[experiments['VarainteProcess']=='STD']
    experiments=experiments.loc[simulation.index]

    map_entity={'lactis':'lactis',
                'plantarum':'plant',
                'freudenreichii':'freud',
                'EX_gal_e':'Galactose-HPLC', 
                'EX_lac__L_e':'Lactate-HPLC',
                'EX_lcts_e':'Lactose-HPLC', 
                'EX_pyr_e':'Pyruvate-HPLC',
                'EX_succ_e':'Succinate-HPLC', 
                #'EX_lac__D_e': 'Lactate-HPLC',
                'EX_diact_e':'Diacétyle', 
                'EX_cit_e':'Citrate-HPLC', 
                'EX_ac_e':'Acétate-HPLC',
                'EX_ppa_e':'Propanoate-HPLC', 
                'ph':'pH'}

    bact =['lactis','plantarum','freudenreichii','ph']
    absolute=['EX_lcts_e','EX_lac__L_e','EX_ppa_e','EX_ac_e','EX_succ_e','EX_cit_e']

    results_w['EX_lac__L_e']=results_w['EX_lac__L_e']+results_w['EX_lac__D_e']
                

                
    experiment_time_series={}
    for key in map_entity.keys():
        current_col = [l for l in experiments.columns if map_entity[key] in l]
        curr_pd = experiments[current_col]
        if key in ['lactis','plantarum','freudenreichii']:
            convert_cfu_per_g_to_g_per_g=0.33*1e-12
            curr_pd = curr_pd.apply(lambda x:(10**x)*convert_cfu_per_g_to_g_per_g,axis=0)
        if key in absolute and key !='ph':
            curr_pd = curr_pd.apply(lambda x:dFBA.convert_g_kg_in_mmol_g(x,molar_mass=param['molar_mass'][key]),axis=0)
            
        experiment_time_series[key] = {'mean':curr_pd.mean(axis=1),
                                        'std':curr_pd.std(axis=1),
                                        'indiv':curr_pd}

#    for b in bact:
#        print(b,'inoculum',experiment_time_series[b]['mean']/convert_cfu_per_g_to_g_per_g)
#        print(b,'log10 biomass',np.log10(experiment_time_series[b]['mean']))
#    for b in absolute:
#        if b != 'ph':
#            print(b,'inoculum',experiment_time_series[b]['mean']*param['molar_mass'][b],experiment_time_series[b]['mean'])



    return results_w, experiment_time_series, simulation, bact, absolute, time, time_o





def Set_annotate(ax,time_o,y=0,
                    offsety={'Linoc':-8,'Finoc':-8,'Cm':-8,'Cdm':-8,'C0w':-8,'C4w':-3.5,'C7w':-3.5},
                    offsetx={'Linoc':-20,'Finoc':-10,'Cm':10,'Cdm':16,'C0w':40,'C4w':10,'C7w':10},
                    shrinkA={'Linoc':2,'Finoc':2,'Cm':2,'Cdm':2,'C0w':2,'C4w':0,'C7w':0},
                    angleB=-90,ha='right',va='top',fontsize=7):
    trans = transforms.blended_transform_factory(ax.transData, ax.transAxes)
    for time_idx,time_loc in zip(time_o.loc[plot_o].index,time_o.loc[plot_o]['time']):
        an = ax.annotate('',#map_time[time_idx],
                            xy=(time_loc,y),
                            xycoords=trans,                            
                            xytext = (offsetx[map_time[time_idx]],offsety[map_time[time_idx]]),
                            textcoords='offset points',
                            ha='right',
                            va='top',
                            rotation=45,
                            rotation_mode='default',
                            arrowprops=dict(lw=0.5,
                            connectionstyle='arc,angleA=0,angleB='+str(angleB)+',armA=0,armB=3.5,rad=0.0',
                            arrowstyle='-',
                            shrinkA=shrinkA[map_time[time_idx]],
                            patchA=None,
                            shrinkB=0,
                            clip_on=False),#
                            annotation_clip = False
                            )
        ax.annotate(map_time[time_idx], xy=(time_loc,y),
                            xycoords=trans,                            
                            xytext = (offsetx[map_time[time_idx]],offsety[map_time[time_idx]]),
                            textcoords='offset points',
                            ha=ha, va=va,  rotation=45,
                            rotation_mode='anchor',fontsize=fontsize)










    
results_w, experiment_time_series, simulation, bact, absolute, time, time_o = get_data_com()

map_name={'lactis':r'$L.lactis$',
                'plantarum':r'$L.plantarum$',
                'freudenreichii':r'$P.freudenreichii$',
                'EX_gal_e':'Galactose', 
                'EX_lac__L_e':'Lactate', 
                'EX_lcts_e':'Lactose', 
                'EX_pyr_e':'Pyruvate',
                'EX_succ_e':'Succinate', 
                'EX_lac__D_e': 'Lactate',
                'EX_diact_e':'Diacetyl', 
                'EX_cit_e':'Citrate',
                'EX_ac_e':'Acetate',
                'EX_ppa_e':'Propionate', 
                'ph':'pH'}
                
map_time = {
                'Linoc':'Linoc',
                'Finoc':'Finoc',
                'Moul':'Cm',
                'Demoul':'Cdm',
                'AS':'C0w',
                'Aff4s':'C4w',
                'Aff7s':'C7w'
                }


plot_exp=['Linoc','Moul','Demoul','AS','Aff4s','Aff7s']
plot_o=['Linoc','Finoc','Moul','Demoul','AS','Aff4s','Aff7s']
color_c={'exp':'C1','sim':'C2'}
final_temp_simul='Aff7s'

##### Figure

## General format
mm = 1.0/25.4 # mm to inches
width = 175.0 #mm
aspect_ratio=2.0

ncols=2
square_size= width*mm/ncols
    
SMALL_SIZE = 7
MEDIUM_SIZE = 8
BIGGER_SIZE = 9
font={'small':SMALL_SIZE,'medium':MEDIUM_SIZE,'big':BIGGER_SIZE}
plt.rc('font', family='Arial',size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes',titlesize=MEDIUM_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=SMALL_SIZE)    # fontsize of the x and y labels
plt.rc('xtick',labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=MEDIUM_SIZE)  # fontsize of the figure title
    
hatch=''

nraws=3

fig = plt.figure(figsize=((ncols*square_size,aspect_ratio*square_size)),constrained_layout=True)
    
gs = fig.add_gridspec(1,ncols)
gs_l=[]
nraw_l=[len(bact),len(absolute)]
for i_col in range(ncols):
    gs_l.append(gs[i_col].subgridspec(nraw_l[i_col],1))
axs = {}

for i_b,b in enumerate(bact):
    axs[b] = fig.add_subplot(gs_l[0][i_b])
for i_m,m in enumerate(absolute):
    axs[m] = fig.add_subplot(gs_l[1][i_m])


abc='abcdefghijklmnopqrst'

for i_l,l in enumerate([bact,absolute]):

    for i_b,b in enumerate(l):
        
        for rep_idx,i_rep in enumerate(experiment_time_series[b]['indiv'].columns):
            if rep_idx==0:
                axs[b].plot(simulation['time'].loc[plot_exp], experiment_time_series[b]['indiv'][i_rep].loc[plot_exp],marker='o',markersize=2,c=color_c['exp'],zorder=0,lw=0.5,label='replicate')
            else:
                axs[b].plot(simulation['time'].loc[plot_exp], experiment_time_series[b]['indiv'][i_rep].loc[plot_exp],marker='o',markersize=2,c=color_c['exp'],zorder=0,lw=0.5)



        if b == 'freudenreichii':
            idx=np.logical_and(results_w['time']<=time.loc[final_temp_simul].values[0],time_o.loc['Finoc'].values[0]<=results_w['time'])
        else:
            idx=results_w['time']<=time.loc[final_temp_simul].values[0]
        axs[b].plot(results_w[idx]['time'],
                    results_w[idx][b],c=color_c['sim'],label='model')
        axs[b].set_xticks([])
        if b in ['lactis','EX_lcts_e']:
            trans = transforms.blended_transform_factory(axs[b].transAxes, axs['lactis'].transAxes)
            axs[b].text(0.5,1.3,map_name[b],fontsize=MEDIUM_SIZE,ha='center',va='center',transform = trans)
        else:
            axs[b].set_title(map_name[b])
        if b in ['plantarum','lactis','freudenreichii']:
            axs[b].set_ylabel(r'density $[g.L^{-1}]$')
        elif 'ph' not in b:
            axs[b].set_ylabel(r'density $[mmol.L^{-1}]$')
        else:
            axs[b].set_ylabel(r'pH $[-]$')        
        if b in ['freudenreichii','plantarum','lactis']:
            axs[b].set_yscale('log')
        axs[b].text(-0.05,1.04, '('+abc[i_l*4+i_b]+')',horizontalalignment='center',verticalalignment='center', transform=axs[b].transAxes,fontsize=font['big'],fontweight='bold')
    axs[l[-1]].set_xticks([])
    axs[l[-1]].set_xticklabels([])
    offsetx={'Linoc':-20,'Finoc':-10,'Cm':10,'Cdm':16,'C0w':40,'C4w':0,'C7w':0}
    angle={key:math.copysign(-180,value) for key,value in offsetx.items()}
    offglob = -8
    offsety={'Linoc':offglob,'Finoc':offglob,'Cm':offglob,'Cdm':offglob,'C0w':offglob,'C4w':-3.5,'C7w':-3.5}
    Set_annotate(axs[l[-1]],time_o,offsetx=offsetx,offsety=offsety,fontsize=SMALL_SIZE)



    if b in ['ph','EX_cit_e']:
        trans = transforms.blended_transform_factory(axs[b].transAxes, axs['ph'].transAxes)
        axs[b].text(0.5,-0.3,r'time $[h]$',fontsize=SMALL_SIZE,ha='center',va='center',transform = trans)

    axs['EX_cit_e'].legend()

    #axs[l[-1]].set_xlabel(r'time $[h]$')


    axs[l[0]].set_xticks([])
    axs[l[0]].set_xticklabels([])
    offglob=10
    offsety={'Linoc':offglob,'Finoc':offglob,'Cm':offglob,'Cdm':offglob,'C0w':offglob,'C4w':3.5,'C7w':3.5}
    Set_annotate(axs[l[0]],time_o,offsetx=offsetx,offsety=offsety,y=1,angleB=90,ha='left',va='bottom',fontsize=SMALL_SIZE)    
    
    
    
    
fig.savefig('community_plot.pdf')
