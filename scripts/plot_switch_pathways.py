import gzip
import pickle

import matplotlib.pyplot as plt
from matplotlib.lines import Line2D

tagatose = ["6PGALSZ","TGBPA","GAL6PI","PFK_2"]
leloir = ["LACZ","GALKr","PGMT","HEX1"]
glycolysis = ["PGI","FBP","FBA","GAPD","PGK","PGM","ENO"]
acetolactate = ["ACLS","ACLD","ACLDC","BTDD_RR","ACTD2"]
ww = ["2131pyrpp","PTA2","PPAKr","PPCSCT","MMM2","MME"]
tca = ["CS","FUM","FRD2rpp","OOR3r","ACONT","ICDHyr"]
transketolase = ["G6PDH2r","PGL","GND","RPE","phosphoketolase"]
pathways = [tagatose,leloir,glycolysis,acetolactate,ww,tca,transketolase]
interest_time = ["0","18.0","19.5","40.0","60.0","632.0","1132.5"]
pathways_str = ["tagatose","leloir","glycolysis","acetolactate","ww","tca","transketolase"]

colors = ['b', 'g', 'r', 'c', 'y', 'k']
lines_style = ["solid","dashed","dotted","dashdot","solid","dashed","dotted"]
marker_list = list(Line2D.markers.keys())
for b in ["freudenreichii","lactis","plantarum"]:
    all_fluxes = {}
    for t in interest_time:
        for idx,p in enumerate(pathways):
            if pathways_str[idx] not in all_fluxes:
                all_fluxes[pathways_str[idx]]={}
            for r in p:
                with gzip.open("results/total_flux_"+b+"_t_"+t+".pkz","rb") as f:
                    A = pickle.load(f)
                    if r in A["result_"+b+"_t_"+t].fluxes:
                        if r not in all_fluxes[pathways_str[idx]]:
                            all_fluxes[pathways_str[idx]][r]=[]
                            first_flux_value = A["result_"+b+"_t_"+t].fluxes[r]
                            if first_flux_value != -0.0:
                                all_fluxes[pathways_str[idx]][r].append(A["result_"+b+"_t_"+t].fluxes[r]/first_flux_value)
                            else:
                                all_fluxes[pathways_str[idx]][r].append(A["result_"+b+"_t_"+t].fluxes[r])
                        else:
                            if first_flux_value != -0.0:
                                all_fluxes[pathways_str[idx]][r].append(A["result_"+b+"_t_"+t].fluxes[r]/first_flux_value)
                            else:
                                all_fluxes[pathways_str[idx]][r].append(A["result_"+b+"_t_"+t].fluxes[r])
    
    if b == "freudenreichii":
        del all_fluxes['tagatose']
        del all_fluxes['transketolase']
        del all_fluxes['acetolactate']
    
    elif b == "lactis":
        del all_fluxes['transketolase']

    elif b == "plantarum":
        del all_fluxes['tagatose']
        del all_fluxes['tca']

    fig,ax=plt.subplots()
    for p,rr in all_fluxes.items():
        i = list(all_fluxes.keys()).index(p)
        for r,flux in rr.items():
            idx_r = list(rr.keys()).index(r)
            ax.plot(interest_time,flux,color = colors[i],label=r,marker=marker_list[idx_r],ls="dotted")
    handles, labels = plt.gca().get_legend_handles_labels()
    by_label = dict(zip(labels, handles))
    box = ax.get_position()
    ax.set_position([box.x0, box.y0 + box.height * 0.1,
                 box.width, box.height * 0.9])
    plt.legend(by_label.values(), by_label.keys(),loc='upper center', bbox_to_anchor=(0.5, -0.15),
          fancybox=True, shadow=True, ncol=5)
    ax.set_ylabel('Fluxes[mmm.gDW.Hr]')
    ax.set_xlabel('Cheese production time [Hr]')
    ax.set_title('Evolution of reaction fluxes for '+b)
    plt.savefig('dynamic_fluxes_'+b+'.pdf',bbox_inches='tight')       
                        
