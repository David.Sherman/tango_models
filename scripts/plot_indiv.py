import gzip
import pickle

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from tango_models import dFBA


def get_data():

    bact= ['lactis','plantarum','freudenreichii']
    results_w={b:None for b in bact}
    param={b:None for b in bact}
    for b in bact:
        if b in ['lactis','plantarum']:
            file_n = "results/result_pure_culture_model_"+b+".pkz"
            with gzip.open(file_n,'rb') as f:
                A=pickle.load(f)
                param[b]=A['param']
                results_curr=A['result']
                results_w[b] = pd.DataFrame(results_curr,columns=[param[b]['Y_output_id_inv'][i] for i in range(results_curr.shape[1])]).set_index('time')
                results_w[b].index = np.around(results_w[b].index,1)
        else:
            for freud_sim in ['growth','metabolites']:
                file_n = "results/result_pure_culture_model_"+b+'_'+freud_sim+".pkz"
                with gzip.open(file_n,'rb') as f:
                    A=pickle.load(f)
                    param[b]=A['param']
                    results_curr=A['result']
                    results_w[b+'_'+freud_sim] = pd.DataFrame(results_curr,columns=[param[b]['Y_output_id_inv'][i] for i in range(results_curr.shape[1])]).set_index('time')
                    results_w[b+'_'+freud_sim].index = np.around(results_w[b+'_'+freud_sim].index,1)

    results_w['freudenreichii_metabolites']['EX_lac__L_e']+=results_w['freudenreichii_metabolites']['EX_lac__D_e']

    cfu_to_gram=0.33e-12

    data_growth = pd.read_csv('data/data_GrowthExp.csv',sep=',')
    d_type=['numbering','pH']


    Growth = {b:{dt:{'mean':None,'std':None,'indiv_values':None} for dt in d_type} for b in bact}
    scale = {'pH':1.0,'numbering':cfu_to_gram}



    for b in bact:
        for dt in d_type:
            for f in ['mean','std']:
                if dt != 'pH' or b != 'freudenreichii':
                    tmp_growth = data_growth[data_growth['type']==dt].pivot_table(columns=['species','replicate'],index='time')[('value',b)]*scale[dt]
                    Growth[b][dt]['mean']=tmp_growth.mean(axis=1).dropna()
                    Growth[b][dt]['std']=tmp_growth.std(axis=1).dropna()
                    Growth[b][dt]['indiv']=tmp_growth.dropna() #Get the two biological replicates
                    Growth[b][dt]['mean']=Growth[b][dt]['mean'].loc[Growth[b]['numbering']['mean'].index]
                    Growth[b][dt]['std']=Growth[b][dt]['std'].loc[Growth[b]['numbering']['mean'].index]
                    Growth[b][dt]['indiv']=Growth[b][dt]['indiv'].loc[Growth[b]['numbering']['mean'].index]
                    
    data_metabolite_freud = pd.read_csv('data/data_metabolites_freud.tsv',sep=',')
    data_map = {'mean':'mean','std':'std'}
    Conc={}
    for d in data_map.keys():        
        curr_c=data_metabolite_freud.pivot_table(columns=['metabolite'],index='time')[d]
        Conc[data_map[d]]=curr_c.apply(lambda x:dFBA.convert_g_kg_in_mmol_g(x,molar_mass=param['lactis']['molar_mass'][x.name]) if x.name not in ['time'] else x, axis=0)
    Conc['indiv'] = Conc['mean'] #For metabolite dosages in Freudenreichii monoculture, only one biological replicate has been done. Here, mean and std are computed from technical replicates of this unique biological replicate.

    return results_w, Conc, Growth, bact, d_type
    
results_w,Conc, Growth, bact, d_type = get_data()


##### Figure

## General format
mm = 1.0/25.4 # mm to inches
width = 175.0 #mm
aspect_ratio=1.4

ncols=2
square_size= width*mm/ncols
    
SMALL_SIZE = 7
MEDIUM_SIZE = 8
BIGGER_SIZE = 9
font={'small':SMALL_SIZE,'medium':MEDIUM_SIZE,'big':BIGGER_SIZE}
plt.rc('font', family='Arial',size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes',titlesize=MEDIUM_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=SMALL_SIZE)    # fontsize of the x and y labels
plt.rc('xtick',labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=MEDIUM_SIZE)  # fontsize of the figure title
    
hatch=''

nraws=3

color_c={'exp':'C1','sim':'C2'}

fig_c = {b: plt.figure(figsize=((square_size,1.5*square_size)),constrained_layout=True) for b in bact}
gs_c = {b:fig_c[b].add_gridspec(2,1) for b in bact}

fig = plt.figure(figsize=((ncols*square_size,aspect_ratio*square_size)),constrained_layout=True)
    
gs = fig.add_gridspec(nraws,1)
gs_l=[]
for i_col in range(nraws):
    gs_l.append(gs[i_col].subgridspec(1,ncols))



axs = { b : [fig.add_subplot(gs_l[i_b][i]) for i,_ in enumerate(gs_l[i_b])] for i_b,b in enumerate(bact)}
axs_c = {b: [fig_c[b].add_subplot(gs_c[b][i]) for i in range(2)] for i_b,b in enumerate(bact)}
for i_b,b in enumerate(bact):
    d_type_map={'pH':'ph','numbering':b}
    for i_t,dt in enumerate(d_type):
        print(i_t,dt)
        for i_ax,ax in enumerate([axs[b][i_t],axs_c[b][i_t]]):        
            if dt != 'pH' or b != 'freudenreichii':
                if b == 'freudenreichii':
                    tag = b+'_growth'
                else:
                    tag = b
                ax.plot(results_w[tag].index,results_w[tag][d_type_map[dt]],c=color_c['sim'],zorder=1,label='model')
                if dt=='numbering':
                    for rep_idx,i_rep in enumerate(Growth[b][dt]['indiv'].columns):
                        if rep_idx==0:
                            ax.plot(Growth[b][dt]['mean'].index,Growth[b][dt]['indiv'][i_rep],marker='o',markersize=2,c=color_c['exp'],zorder=0,lw=0.5,label=Growth[b][dt]['indiv'].columns.name)
                        else:
                            ax.plot(Growth[b][dt]['mean'].index,Growth[b][dt]['indiv'][i_rep],marker='o',markersize=2,c=color_c['exp'],zorder=0,lw=0.5)
                else:
                    for i_rep in Growth[b][dt]['indiv'].columns:
                        ax.plot(Growth[b][dt]['mean'].index,Growth[b][dt]['indiv'][i_rep],marker='o',c=color_c['exp'],zorder=1,markersize=2,lw=0.5,label=Growth[b][dt]['indiv'].columns.name+' '+str(i_rep))

                ax.set_xlabel(r'time $[h]$')
            if dt == 'numbering':
                ax.set_title('growth')
                if i_ax==0:
                    ax.set_ylabel(b+'\n'+r'density $[g.L^{-1}]$')
                else:
                    ax.set_ylabel(r'density $[g.L^{-1}]$')
                 
            else:
                ax.set_title('pH')
                ax.set_ylabel(r'pH $[-]$')
        axs_c[b][0].legend()             
        axs[b][0].set_yscale('log')
        axs_c[b][0].set_yscale('log')
time=89.0

for ax in [axs['freudenreichii'][1],axs_c['freudenreichii'][1]]:
    ax.scatter(Conc['mean'].columns,Conc['mean'].loc[time],label='replicate', marker='o', color=color_c['exp'], s=5, zorder=1)
    #ax.errorbar(Conc['mean'].columns,Conc['mean'].loc[time],yerr=Conc['std'].loc[time],fmt='x',ecolor=color_c['exp'],capsize=1,color=color_c['exp'],zorder=1) 
    ax.scatter(Conc['mean'].columns,results_w['freudenreichii_metabolites'].loc[time][Conc['mean'].columns],label='model',marker='o',color=color_c['sim'],s=5,zorder=2)            
    ax.set_ylabel(r"concentration $[mmol.L^{-1}]$")
    ax.set_xlabel("metabolites")
    ax.set_title("metabolites")
    ax.legend()
fig.savefig('Result_monoculture.pdf')
fig.savefig('Result_monoculture.svg')

for b in bact:
    fig_c[b].savefig('Result_monoculture_'+b+'.pdf')
    fig_c[b].savefig('Result_monoculture_'+b+'.svg')

