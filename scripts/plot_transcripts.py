import gzip
import pickle

import cobra
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


def get_data_flux():

    with gzip.open('results/result_community_model_whole.pkz','rb') as f:
        A=pickle.load(f)
        param=A['param']
        results_w=A['result']
        results_w = pd.DataFrame(results_w,columns=[param['Y_output_id_inv'][i] for i in range(results_w.shape[1])])
        results_flux = A['flux']
        results_flux = {i : pd.DataFrame(results_flux[i],columns=[param['Y_output_id_inv'][i] for i in range(results_flux[i].shape[1])]) for i in results_flux.keys()}
    return results_w, results_flux, param
results_w, results_flux, param=get_data_flux()

list_time = ['mMT','mDT','m4T','mAT','m7T']
list_quantiles = [r.replace('m','Q') for r in list_time]

path_models = 'metabolic_models/'
path_trans = 'data/data_transcript/'
dict_models = {'freudenreichii':'freudenreichii.sbml','lactis':'lactis.sbml','plantarum':'plantarum.sbml'}
list_bact = param['bact']
#list_bact = ['freudenreichii']

dict_transcripts = {'plantarum':'DataNormLP_RPKM_meaned.tsv',
                    'freudenreichii':'DataNormPF_RPKM_meaned.tsv',
                    'lactis':'DataNormLL_RPKM_meaned.tsv'}

dict_reactions = {'lactis':['LACZ','6PGALSZ'],
                    'plantarum':['LACZ'],
                    'freudenreichii':['LACZ','LDH_D','LDH_L']
                    }

gene_id_dict={'freudenreichii':'PFREU_v1',
                'lactis':'LLACBIA1206_v1',
                'plantarum':'LPLABIA465_v1'}

smetana=pd.read_excel(path_trans+'exchanged_metabolites_smetana.xlsx').iloc[:,:-12].dropna().drop_duplicates(subset=['locus','locus species'])

smetana_dict = { i : {'locus':[gene_id_dict[i]+'_'+"%04d" % int(r) for r in smetana[smetana['locus species']==i]['locus']],'name': [r for r in smetana[smetana['locus species']==i]['reaction']] } for i in smetana['locus species'].unique()}

#### Figure

## General format
mm = 1.0/25.4 # mm to inches
width = 175.0 #mm
aspect_ratio=3.0

ncols=1
square_size= width*mm/ncols
    
SMALL_SIZE = 5
MEDIUM_SIZE = 6
BIGGER_SIZE = 7
font={'small':SMALL_SIZE,'medium':MEDIUM_SIZE,'big':BIGGER_SIZE}
plt.rc('font', family='Arial',size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes',titlesize=MEDIUM_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=SMALL_SIZE)    # fontsize of the x and y labels
plt.rc('xtick',labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=MEDIUM_SIZE)  # fontsize of the figure title



### Taken from https://matplotlib.org/stable/gallery/images_contours_and_fields/image_annotated_heatmap.html    
def heatmap(data, row_labels, col_labels, ax=None,
            cbar_kw=None, cbarlabel="", **kwargs):
    """
    Create a heatmap from a numpy array and two lists of labels.

    Parameters
    ----------
    data
        A 2D numpy array of shape (M, N).
    row_labels
        A list or array of length M with the labels for the rows.
    col_labels
        A list or array of length N with the labels for the columns.
    ax
        A `matplotlib.axes.Axes` instance to which the heatmap is plotted.  If
        not provided, use current axes or create a new one.  Optional.
    cbar_kw
        A dictionary with arguments to `matplotlib.Figure.colorbar`.  Optional.
    cbarlabel
        The label for the colorbar.  Optional.
    **kwargs
        All other arguments are forwarded to `imshow`.
    """

    if ax is None:
        ax = plt.gca()

    if cbar_kw is None:
        cbar_kw = {}

    # Plot the heatmap
    im = ax.imshow(data, **kwargs)

    # Create colorbar
    cbar = ax.figure.colorbar(im, ax=ax, **cbar_kw)
    cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom")

    # Show all ticks and label them with the respective list entries.
    ax.set_xticks(np.arange(data.shape[1]), labels=col_labels)
    ax.set_yticks(np.arange(data.shape[0]), labels=row_labels)

    # Let the horizontal axes labeling appear on top.
    ax.tick_params(top=True, bottom=False,
                   labeltop=True, labelbottom=False)

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=-30, ha="right",
             rotation_mode="anchor")

    # Turn spines off and create white grid.
    ax.spines[:].set_visible(False)

    ax.set_xticks(np.arange(data.shape[1]+1)-.5, minor=True)
    ax.set_yticks(np.arange(data.shape[0]+1)-.5, minor=True)
    ax.grid(which="minor", color="w", linestyle='-', linewidth=3)
    ax.tick_params(which="minor", bottom=False, left=False)

    return im, cbar


fig,ax = plt.subplots(1,1,figsize=((0.7*square_size,1.2*square_size)))    

total_df = pd.DataFrame(columns=list_quantiles+['bact'],dtype=int)
total_geneid=[]

for i_bact,bact in enumerate(list_bact):
    mod = dict_models[bact]
    curr_mod = cobra.io.read_sbml_model(path_models+mod)
    curr_trans = pd.read_csv(path_trans+dict_transcripts[bact],sep='\t').set_index('geneid')[list_time]
    for c in curr_trans.columns:
        curr_trans[c.replace('m','Q')] = pd.qcut(curr_trans[c],10,labels=False,duplicates='drop')+1
    list_gene=[]
    list_geneid=[]
    for r in dict_reactions[bact]:
        curr_r = curr_mod.reactions.get_by_id(r)
        for g in curr_r.genes:
            list_gene.append('_'.join([k.replace('','') for k in g.id.split('_')[:3]]))
            list_geneid.append(curr_r.name+' ('+r+')')
    if bact in smetana_dict.keys():
        for gene,gene_name in zip(smetana_dict[bact]['locus'],smetana_dict[bact]['name']):
            list_gene.append(gene)
            list_geneid.append(gene_name)       
    curr_trans = curr_trans.loc[list_gene]
    curr_trans['reaction']=list_geneid
    curr_trans['bact']=bact
    total_geneid+=list_geneid
    total_df = pd.concat([total_df,curr_trans[list_quantiles+['bact']]],axis=0)
    curr_trans.to_csv(path_trans+bact+'_'+'lactose.csv',index=True,header=True,sep='\t')
    
    

norm = mpl.colors.BoundaryNorm(np.linspace(0.5,10.5, 11), 10)


im, _ = heatmap(total_df[list_quantiles],  total_geneid,['Cm','Cdm','C0w','C4w','C7w'], ax=ax,
                    cmap=mpl.colormaps["Reds"].resampled(10), norm=norm,
                    cbar_kw=dict(ticks=np.arange(1, 11),shrink=0.5),
                    cbarlabel="Transcript RPKM decile",origin='upper')

for i_bact,bact in enumerate(list_bact):
    curr_ind = np.arange(total_df.shape[0])[total_df['bact']==bact]
    print(curr_ind)
    width=21
    pad=0.5
    boxes = [mpl.patches.Rectangle((- width-pad, min(curr_ind)-pad), width, max(curr_ind)-min(curr_ind)+2*pad)]

    # Create patch collection with specified colour/alpha
    pc = mpl.collections.PatchCollection(boxes, facecolor='C'+str(i_bact), alpha=0.2,
                         edgecolor='k',clip_on=False)

    # Add collection to axes
    ax.add_collection(pc)
    ax.text( -width+0.3,(max(curr_ind)+min(curr_ind))/2.0,bact,horizontalalignment='center',verticalalignment='center',rotation=90,fontsize=BIGGER_SIZE,fontweight='bold')
fig.savefig('heatmap_transcriptomics.pdf')   

