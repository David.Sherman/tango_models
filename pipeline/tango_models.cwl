cwlVersion: v1.2
class: CommandLineTool

label: Numerical reconciliation of bacterial fermentation in cheese production
doc: |-
  TANGO uses a numerical strategy to reconcile multi-omics data and
  metabolic networks for characterising bacterial fermentation in
  cheese production composed of 3 species:
  *P. freudenreichii*, *L. lactis* and *L. plantarum*.

requirements:
  ShellCommandRequirement: {}
  InlineJavascriptRequirement: {}
  InitialWorkDirRequirement:
    listing:
      - $(inputs.data)
      - $(inputs.initial_res_optim)
      - basename: "results"
        class: Directory
        listing: []
      - basename: "pipeline"
        class: Directory
        listing: []
  NetworkAccess:
    class: NetworkAccess
    networkAccess: true

hints:
  DockerRequirement:
    dockerImageId: tango_models
    dockerFile: |-
      FROM mambaorg/micromamba
      ADD environment-minimal.yml .

      RUN micromamba install --yes \
            --name base -c bioconda -c conda-forge \
            -f environment-minimal.yml \
          micromamba clean --all --yes

      ARG MAMBA_DOCKERFILE_ACTIVATE=1

inputs:
  cobra_solver:
    doc: |-
      solver used for FBA computation by Cobra
      {glpk,glpk_exact,cplex,scipy}
    type:
      # - "null"
      - type: enum
        symbols:
          - "glpk"
          - "glpk_exact"
          - "cplex"
          - "scipy"
    default: "glpk"
    inputBinding:
      position: 5
      prefix: "-CobraSolver"

  community_scale:
    doc: |-
      dFBA at the community scale {True,False}
    type:
      # - "null"
      - type: enum
        symbols:
          - "True"
          - "False"
    default: "False"
    inputBinding:
      position: 5
      prefix: "-com"

  culture:
    type: File
    format: edam:format_3750
    doc: |-
      specific media and/or specif modification applied to the FBA
      models
    default:
      class: File
      location: "config_file/config_culture.yml"
      format: http://edamontology.org/format_3750
    inputBinding:
      position: 2
      prefix: "-cp"

  data:
    type: Directory
    doc: |-
      Directory containing experimental data, referenced in optimize
      configuration
    default:
      class: Directory
      location: ../data

  dynamics:
    type: File
    format: edam:format_3750
    doc: |-
      specific paramters for your dFBA analysis
    default:
      class: File
      location: "config_file/config_dynamic.yml"
      format: http://edamontology.org/format_3750
    inputBinding:
      position: 3
      prefix: "-dp"

  freud_sim:
    doc: |-
      different initial conditions for lactate where defined in the
      experiments for growth and metabolite dosage => this parameter
      allows to switch between both situations {growth,metabolites}
    type:
      # - "null"
      - type: enum
        symbols:
          - "growth"
          - "metabolites"
    default: "growth"
    inputBinding:
      position: 5
      prefix: "-fsim"

  lactic_acid_model:
    doc: |-
      lactic acid model use total if lactate represents total lactic
      acid concentration, or dissociated if lactate represents the
      dissociated lactic acid {total,dissociated} 
    type:
      # - "null"
      - type: enum
        symbols:
          - "total"
          - "dissociated"
    default: "total"
    inputBinding:
      position: 5
      prefix: "-lam"

  model:
    type:
      type: array
      items: File
    format: edam:format_2585
    doc: |-
      SBML model
    inputBinding:
      position: 1
      prefix: "-mp"

  optimize:
    doc: |-
      activatee or not the optimization on parameters {True,False}
    type:
      # - "null"
      - type: enum
        symbols:
          - "True"
          - "False"
    default: "False"
    inputBinding:
      position: 5
      prefix: "-optim"

  recovery:
    doc: |-
      activate or not recovery of the optimization on parameters
      {True,False}
    type:
      # - "null"
      - type: enum
        symbols:
          - "True"
          - "False"
    default: "False"
    inputBinding:
      position: 6
      prefix: "-r"

  initial_res_optim:
    type: File?
    doc: |-
      Per-species optimization results
    # default:
    #   class: File
    #   location: ./res_optim.txt

  solver:
    type: File
    format: edam:format_3750
    doc: |-
      configuration file for optimization
    default:
      class: File
      location: "config_file/config_optim.yml"
      format: http://edamontology.org/format_3750
    inputBinding:
      position: 4
      prefix: "-sp"

  verbose:
    doc: |-
      active or not verbose reporting {True,False}
    type:
      # - "null"
      - type: enum
        symbols:
          - "True"
          - "False"
    default: "False"
    inputBinding:
      position: 6
      prefix: "-v"

baseCommand:
  - tango_models
  - sim

outputs:
  results:
    type: File[]
    outputBinding:
      glob:
        - "results/*.pkz"
        - "results/*.csv"
      outputEval: |
         ${
             return self.filter(function(i) { return -1 == i.basename.indexOf("_t_") })
         }
    doc: |
      Pickled Pandas data frames containing simulation results
    format: edam:format_2333
  recovery_optimisation:
    type: File?
    outputBinding:
      glob: "recover_optim*yml"
    doc: |-
      Pickle file of recovery optimization
    format: edam:format_2333
  result_optimization:
    type: File
    outputBinding:
      glob: "res_optim.txt"
    doc: |-
      optimization results
    format: edam:format_3475
  standard_output:
    type: stdout
    format: edam:format_1964
  standard_error:
    type: stderr
    format: edam:format_1964
  time_series:
    type: File[]
    outputBinding:
      glob:
        - "results/*.pkz"
      outputEval: |
         ${
             return self.filter(function(i) { return -1 != i.basename.indexOf("_t_") })
         }
    doc: |
      Pickled Pandas data frames containing time series of simulation results
    format: edam:format_2333

stdout: stdout.txt
stderr: stderr.txt
 
s:author:
  - class: s:Person
    s:name: Simon Labarthe
    s:identifier: https://orcid.org/0000-0003-2114-0697
  - class: s:Person
    s:name: Clémence Frioux
    s:identifier: https://orcid.org/0000-0003-2114-0697
  - class: s:Person
    s:name: David James Sherman
    s:identifier: https://orcid.org/0000-0002-2316-1005
  - class: s:Person
    s:name: Maxime Lecomte
    s:identifier: https://orcid.org/0000-0002-4558-6151
  - class: s:Person
    s:name: Hélène Falentin
    s:identifier: https://orcid.org/0000-0001-6254-5303
  - class: s:Person
    s:name: Julie Aubert
    s:identifier: https://orcid.org/0000-0001-5203-5748

s:citation: https://doi.org/10.1016/j.ymben.2024.02.014
s:codeRepository: https://forgemia.inra.fr/tango/tango_models.git
s:license: https://spdx.org/licenses/LGPL-3.0-or-later
s:programmingLanguage: Python
s:dateCreated: "2024-03-02"
 
$namespaces:
  s: https://schema.org/
  edam: http://edamontology.org/
$schemas:
 - https://schema.org/version/latest/schemaorg-current-http.rdf
 - http://edamontology.org/EDAM_1.23.owl
