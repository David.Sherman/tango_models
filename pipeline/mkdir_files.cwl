---
cwlVersion: v1.2
class: ExpressionTool

doc: |
  Combine an array of Files into a single named directory. If
  files with duplicate basenames are provided, only the first
  one is included, whether or not their contents are the same.

requirements:
  InlineJavascriptRequirement: {}
  MultipleInputFeatureRequirement: {}
  LoadListingRequirement: { loadListing: shallow_listing }

inputs:
  name:
    type: string
    default: "results"
  file_list:
    type:
      - File[]
      - Directory

outputs:
  out_dir: Directory

expression: |
  ${
    var file_list = inputs.file_list
    if (file_list.class == "Directory") {
      file_list = file_list.listing
    }
    var flat = []
    var seen = {}
    for (var i=0; i<file_list.length; i++) {
      var key = file_list[i].basename
      if (!seen.hasOwnProperty(key)) {
        flat = flat.concat(file_list[i])
      }
      seen[key] = true
    }
    return {
      "out_dir": {
        "class": "Directory",
        "basename": inputs.name,
        "listing": flat
      }
    }
  }
