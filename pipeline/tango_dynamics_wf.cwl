---
cwlVersion: v1.2
class: Workflow

requirements:
  MultipleInputFeatureRequirement: {}
  StepInputExpressionRequirement: {}
  ScatterFeatureRequirement: {}  
  InlineJavascriptRequirement: {}

inputs:
  initial_res_optim: File
  pure_culture:
    type:
      type: array
      items:
        type: record
        fields:
          models: File[]
          freud_sim: string
  community:
    type:
      type: array
      items:
        type: record
        fields:
          models: File[]
  
outputs:
  results:
    type: File[]
    outputSource: combine/flattened
  time_series:
    type: File[]
    outputSource: combine_time_series/flattened

doc: |-
  For each model file in the input model_list, run an individual
  simulation. For each collection of models into a community, run
  a community simulation. Return both summary and time series
  simulation results.

steps:
  
  individual_each:
    run: tango_models.cwl
    scatter: [model, freud_sim]
    scatterMethod: dotproduct
    in:
      model:
        source: pure_culture
        valueFrom: $([self.models[0]])  # wrap model in singleton array
      freud_sim:
        source: pure_culture
        valueFrom: $(self.freud_sim)
      optimize:
        valueFrom: "False"
      community_scale: 
        valueFrom: "False"
      initial_res_optim:
        source: initial_res_optim
    out: [results, time_series]

  community_together:
    run: tango_models.cwl
    scatter: [model, culture, dynamics, solver]
    scatterMethod: dotproduct
    in:
      model:
        source: community
        valueFrom: $(self.models)
      optimize:
        valueFrom: "False"
      community_scale: 
        valueFrom: "True"
      initial_res_optim:
        source: initial_res_optim
      culture:
        source: community
        valueFrom: $(self.culture || null)
      dynamics:
        source: community
        valueFrom: $(self.dynamics || null)
      solver:
        source: community
        valueFrom: $(self.solver || null)
    out: [results, time_series]

  combine:
    run: flatten_array.cwl
    in:
      nested:
        source: [individual_each/results, community_together/results]
        linkMerge: merge_flattened
    out: [flattened]

  combine_time_series:
    run: flatten_array.cwl
    in:
      nested:
        source: [individual_each/time_series, community_together/time_series]
        linkMerge: merge_flattened
    out: [flattened]
